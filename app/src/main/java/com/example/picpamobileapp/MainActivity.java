package com.example.picpamobileapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.picpamobileapp.Activities.LoginActivity;
import com.example.picpamobileapp.Activities.SeminarsTabActivity;
import com.example.picpamobileapp.Activities.UserProfile;
import com.example.picpamobileapp.Fragments.CpdMonitoringFragment;
import com.example.picpamobileapp.Fragments.EvaluationFragment;
import com.example.picpamobileapp.Fragments.HomeFragment;
import com.example.picpamobileapp.Fragments.MyProfileFragment;
import com.example.picpamobileapp.Fragments.UpseminarsFragment;
import com.example.picpamobileapp.Utils.Migration;
import com.example.picpamobileapp.Utils.PopUpProvider;
import com.example.picpamobileapp.Utils.UserSession;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private View navHeadView;
    private Context context;
    private Realm realm;
    //private TextView tvUserEmail, tvCompanyName;
    FragmentManager fragmentManager;
    private com.example.picpamobileapp.Models.UserProfile userProfile;
    private int selectedNavId;
    private long backPressedTime;
    private DrawerLayout drawer;
    public static Toolbar mToolbar;
    private ImageView imgProfile;
    private TextView tvName, tvEmail;

    private boolean flag = false;
    //private SearchView searchView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        // The RealmConfiguration is created using the builder pattern.
        // The Realm file will be located in Context.getFilesDir() with name "picpa_timekeeper.realm"
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("picpa_mobile.realm")
                .schemaVersion(3)
                .migration(new Migration())
                .build();
        Realm.setDefaultConfiguration(config);
        fragmentManager = getSupportFragmentManager();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                switchFragment();
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navHeadView = navigationView.getHeaderView(0);
        imgProfile = navHeadView.findViewById(R.id.imgProfile);
        tvName = navHeadView.findViewById(R.id.tvName);
        tvEmail = navHeadView.findViewById(R.id.tvEmail);


        try {
            realm = Realm.getDefaultInstance();
            userProfile = realm.where(com.example.picpamobileapp.Models.UserProfile.class).findFirst();
            if (userProfile != null){
                tvName.setText(userProfile.getFirst_name() + " " + userProfile.getLast_name());
                tvEmail.setText(userProfile.getEmail_add());
            }
            else {

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        navigationView.setCheckedItem(R.id.nav_home);

        checkUserSession();
    }

    private void openUserProfile(){
        Intent intent = new Intent(MainActivity.this, UserProfile.class);
        startActivity(intent);
    }

    private void switchFragment()
    {
        switch (selectedNavId) {
            case R.id.nav_home: {
                openHomeFragment();
                break;
            }

            case R.id.nav_CpdMonitoring: {
                openCpdMonitoringFragment();
                break;
            }

            case R.id.nav_evaluation: {
                openEvaluationFragment();
                break;
            }

            case R.id.nav_seminars: {
                openSeminarsActivity();
                break;
            }

            case R.id.nav_signout: {
                openUpSignOutDialog();
                break;
            }

            case R.id.nav_profile: {
                openMyProfileFragment();
                break;
            }
        }
    }

    private void openUpSignOutDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Sign Out");
        builder.setIcon(R.drawable.ic_exit_to_app_black_24dp);
        builder.setMessage("Are you sure you want to sign out?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                UserSession.clearSession(context);
                deleteRecord();
                checkUserSession();
                //MainActivity.this.finish();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setCancelable(false);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private void deleteRecord()
    {
        realm = Realm.getDefaultInstance();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.deleteAll();
                }
            });
        } finally {
            realm.close();
        }

    }

    private void checkUserSession()
    {
        String authToken = UserSession.getToken(this);

        if (authToken.length() <= 0) {
            Intent login_intent = new Intent(this, LoginActivity.class);
            startActivity(login_intent);
            this.finish();
        } else {
            if (UserSession.getFirstLogin(this) == 1){
                openMyProfileFragment();
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("FIRST_LOGIN", 0);
                editor.commit();
            }
            else {
                openHomeFragment();
            }
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.main);

            if (f instanceof HomeFragment) {
                closeSession();
            } else {
                openHomeFragment();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {

//            case R.id.action_logout :
//                //UserSession.clearSession(context);
//                closeSession();
//                break;
            case R.id.action_search:
                //showSearchBar();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        selectedNavId = menuItem.getItemId();
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void openHomeFragment(){
        HomeFragment homeFragment = new HomeFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main,homeFragment,homeFragment.getTag()).commit();
    }

    public void openCpdMonitoringFragment(){
        CpdMonitoringFragment cpdMonitoringFragment = new CpdMonitoringFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main,cpdMonitoringFragment,cpdMonitoringFragment.getTag()).commit();
    }

    private void openEvaluationFragment() {
        setTitle("Evaluation");
        EvaluationFragment evaluationFragment = new EvaluationFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main,evaluationFragment,evaluationFragment.getTag()).commit();
    }

    private void openUpSeminarsFragment(){
        UpseminarsFragment upseminarsFragment = new UpseminarsFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main,upseminarsFragment,upseminarsFragment.getTag()).commit();
    }

    private void openMyProfileFragment() {

        MyProfileFragment myProfileFragment = new MyProfileFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main,myProfileFragment,myProfileFragment.getTag()).commit();

        com.example.picpamobileapp.Models.UserProfile userProfile1 = realm.where(com.example.picpamobileapp.Models.UserProfile.class).findFirst();
        if (userProfile1 != null){
            tvName.setText(userProfile1.getFirst_name() + " " + userProfile1.getLast_name());
            tvEmail.setText(userProfile1.getEmail_add());
        }
    }



    private void closeSession() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        MainActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(context, dialogClickListener, "Confirm close ", "Are you sure you want to close application?","Yes", "No");
    }

    private void openSeminarsActivity(){

        Intent intent = new Intent(context, SeminarsTabActivity.class);
        startActivity(intent);

    }

}
