package com.example.picpamobileapp.Activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.picpamobileapp.R;

public class RegisterActivity extends AppCompatActivity {
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        context = this;
        initializeUI();
        getSupportActionBar().setElevation(0);
        this.setTitle("Sign Up");
    }

    private void initializeUI(){

    }
}
