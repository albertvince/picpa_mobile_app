package com.example.picpamobileapp.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.picpamobileapp.Adapters.ProfilePagerAdapter;
import com.example.picpamobileapp.Fragments.ProfileCompanyFragment;
import com.example.picpamobileapp.Fragments.ProfileContactDetailsFragment;
import com.example.picpamobileapp.Fragments.ProfilePRCLicenseFragment;
import com.example.picpamobileapp.Fragments.ProfileUserDetailsFragment;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.HttpProvider;
import com.example.picpamobileapp.Utils.Syncer;
import com.example.picpamobileapp.Utils.UserSession;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.mikhaellopez.circularimageview.CircularImageView;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;
import io.realm.Realm;

public class UserProfile extends AppCompatActivity {
    private Context context;
    private Realm realm;
    private ViewPager viewPager;
    private com.example.picpamobileapp.Models.UserProfile userProfile;
    private ProfilePagerAdapter profilePagerAdapter;
    private TabLayout tabLayout;
    private ProgressDialog progressDialog;
    private TextView tvName, tvCPA;
    private Button btnUpload, btnSave;
    private CircularImageView imgProfile;
    private Bitmap bitmap;
    private final int GALLERY_REQUEST_CODE = 999;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        realm = Realm.getDefaultInstance();
        context = getApplicationContext();

        userProfile = realm.where(com.example.picpamobileapp.Models.UserProfile.class).findFirst();

        if (userProfile == null){
            Syncer.syncUserProfile(this, false, this);
        }
        else {
            initializeUI();
        }

        this.setTitle("Edit Profile");
    }

    public void initializeUI(){
        progressDialog = new ProgressDialog(this);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewpager);
        profilePagerAdapter = new ProfilePagerAdapter(getSupportFragmentManager());

        profilePagerAdapter.addFragment(ProfileUserDetailsFragment.newInstance(), "User Details");
        profilePagerAdapter.addFragment(ProfileContactDetailsFragment.newInstance(), "Contact Details");
        profilePagerAdapter.addFragment(ProfilePRCLicenseFragment.newInstance(), "PRC License");
        profilePagerAdapter.addFragment(ProfileCompanyFragment.newInstance(), "Company");

        viewPager.setAdapter(profilePagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        imgProfile = findViewById(R.id.imgProfile);
        btnUpload = findViewById(R.id.btnUpload);
        btnSave = findViewById(R.id.btnSave);

        tvName = findViewById(R.id.tvName);
        tvCPA = findViewById(R.id.tvCPA);

        userProfile = realm.where(com.example.picpamobileapp.Models.UserProfile.class).findFirst();
        tvName.setText(userProfile.getFirst_name() + " " + userProfile.getLast_name() + " " + userProfile.getSuffixes());

        RequestOptions myOption = new RequestOptions().centerInside();

        Glide.with(this)
                .load("https://registration.picpacdomisor.org/userimages/" + userProfile.getUser_image())
                .apply(myOption)
                .into(imgProfile);

        if (userProfile.getRegister_no().isEmpty()){
            tvCPA.setText("Non-CPA");
        }
        else {
            tvCPA.setText("CPA");
        }

        btnUpload = findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        UserProfile.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_REQUEST_CODE);
            }
        });
//        btnSave.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == GALLERY_REQUEST_CODE){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Image"), GALLERY_REQUEST_CODE);
            }
            else {
                Toasty.error(getApplicationContext(), "You don't have permission to access gallery").show();
            }
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void updateImage(Bitmap bitmap){
        userProfile = realm.where(com.example.picpamobileapp.Models.UserProfile.class).findFirst();

        realm.beginTransaction();

        userProfile.setUser_image(imagetoString(bitmap));

        realm.commitTransaction();

        Toasty.success(context, "Saved").show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && data != null){
            Uri filepath = data.getData();

            try {
                InputStream inputStream = getContentResolver().openInputStream(filepath);
                bitmap = BitmapFactory.decodeStream((inputStream));
                imgProfile.setImageBitmap(bitmap);


            } catch (FileNotFoundException e){
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String imagetoString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    //
    private void updateProfile()
    {
        userProfile = realm.where(com.example.picpamobileapp.Models.UserProfile.class).findFirst();
        try
        {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Token", UserSession.getToken(context));
            jsonObject.put("username", userProfile.getUsername());
            jsonObject.put("password", userProfile.getPassword());
            jsonObject.put("first_name", userProfile.getFirst_name());
            jsonObject.put("middle_name", userProfile.getMiddle_name());
            jsonObject.put("last_name", userProfile.getLast_name());
            jsonObject.put("suffixes", userProfile.getSuffixes());
            jsonObject.put("contact_no", userProfile.getContact_no());
            jsonObject.put("facebook_acc", userProfile.getFacebook_acc());
            jsonObject.put("email_add", userProfile.getEmail_add());
            jsonObject.put("full_address", userProfile.getFull_address());
            jsonObject.put("register_no", userProfile.getRegister_no());
            jsonObject.put("register_date", userProfile.getRegister_date());
            jsonObject.put("valid_until", userProfile.getValid_until());
            jsonObject.put("company_name", userProfile.getCompany_name());
            jsonObject.put("sector", userProfile.getSector() + "");
            jsonObject.put("billing_address", userProfile.getBilling_address());
            jsonObject.put("chapter", userProfile.getChapter());
            String imageData = imagetoString(bitmap);
            jsonObject.put("user_image", imageData);

            Debugger.printO(userProfile.getUser_image());

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            Debugger.printO(jsonObject.toString());

            HttpProvider.post(context, "post/memberupdate", stringEntity, "application/json", new JsonHttpResponseHandler(){

                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog.show();
                    progressDialog.setMessage("Saving");

                }

                @Override
                public void onFinish() {
                    super.onFinish();
//                    progressDialog.dismiss();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Debugger.logD(response.toString());
                    progressDialog.hide();
                    Toasty.success(context, "Successfully saved").show();
                    initializeUI();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    progressDialog.hide();
                    Debugger.logD("Success");
                    //Toasty.success(context, "Success").show();
                    // Toast.makeText(getContext(),"Success",Toast.LENGTH_SHORT).show();
                    Debugger.printO(response);
                    Toasty.success(context,"Success  ");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    progressDialog.hide();
                    //Toasty.error(context, "Error From Server 1").show();
                    //Toast.makeText(getContext(),"Error From Server 1",Toast.LENGTH_SHORT).show();
                    Debugger.printO(errorResponse);
                    Debugger.logD("Error From Server 3  1st");
                    Toasty.error(context,"Error From Server 3 ");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    progressDialog.hide();
                    //Toasty.error(context, "Error from Server 2").show();
                    // Toast.makeText(getContext(),"Error From Server 2",Toast.LENGTH_SHORT).show();
//                    Debugger.printO(errorResponse);
                    Debugger.logD("Error From Server 2 ");
                    Toasty.error(context,"Error From Server 2 ");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    progressDialog.hide();
                    //Toasty.error(context, responseString).show();
                    //Toast.makeText(getContext(),"Error From Server 3 " + responseString,Toast.LENGTH_SHORT).show();
                    Debugger.printO(throwable);
                    Debugger.logD("Error From Server 3 last ");
                    Toasty.error(context,"Error From Server 3 ");
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode, headers, responseString);
                    progressDialog.hide();
                    Debugger.logD("On success last");
                    Debugger.printO(responseString);
                }
            });

        } catch (Exception err)
        {
            //Toasty.error(context, err.toString()).show();
            Toast.makeText(context,"Error " + err.toString(),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
//            Toasty.success(getApplicationContext(), "Successfully saved.").show();
            updateProfile();
//            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
}
