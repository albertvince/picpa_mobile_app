package com.example.picpamobileapp.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.picpamobileapp.Models.Evaluation;
import com.example.picpamobileapp.Models.Speakers;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.HttpProvider;
import com.example.picpamobileapp.Utils.UserSession;
import com.example.picpamobileapp.Utils.Utility;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;
import io.realm.Realm;

public class EvaluationActivity extends AppCompatActivity {

    //Context
    private Context context;

    //Widgets
    private EditText etAnswer;
    private RadioGroup rgAnswer;
    private ProgressDialog progressDialog;
    private Button btnBack, btnNext, btnSubmit;
    private RadioButton rbExcellent, rbSatisfactory, rbFair, rbImprovement, rbVerySatisfactory;
    private TextView tvSeminarTitle, tvSpeakerName, tvSpeakerTopic, tvQuestion, tvCountQuestion;

    //Data
    private Realm realm;
    private Speakers speakers;
    private Evaluation evaluation;
    private ArrayList<String> question;
    private ArrayList<String> answers = new ArrayList<>();
    private int incrementQuestion = 1, items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation);
        context = this;
        realm = Realm.getDefaultInstance();

        evaluation = getIntent().getParcelableExtra("SEMINAR_OBJECT");
        speakers = getIntent().getParcelableExtra("SPEAKER_OBJECT");

        initializeUI();
        this.setTitle("Answer Questions");
    }

    private void initializeUI() {

        rbFair = findViewById(R.id.rb_fair);
        btnBack = findViewById(R.id.btn_back);
        btnNext = findViewById(R.id.btn_next);
        rgAnswer = findViewById(R.id.rg_answer);
        etAnswer = findViewById(R.id.et_answer);
        btnSubmit = findViewById(R.id.btn_submit);
        tvQuestion = findViewById(R.id.tv_question);
        rbExcellent = findViewById(R.id.rb_excellent);
        rbImprovement = findViewById(R.id.rb_improvement);
        tvSpeakerName = findViewById(R.id.tv_speaker_name);
        rbSatisfactory = findViewById(R.id.rb_satisfactory);
        tvSeminarTitle = findViewById(R.id.tv_seminar_title);
        tvSpeakerTopic = findViewById(R.id.tv_speaker_topic);
        tvCountQuestion = findViewById(R.id.tv_count_question);
        rbVerySatisfactory = findViewById(R.id.rb_very_satisfactory);

        etAnswer.setVisibility(View.GONE);
        tvSeminarTitle.setText(evaluation.getTopic());
        tvSpeakerTopic.setText(speakers.getSeminartopic());
        tvSpeakerName.setText(speakers.getSeminarspeaker());

        answerQuestions();
    }

    private void answerQuestions(){

        question = new ArrayList<>();
        question.add("");
        question.add("Mastery of the subject matter, including examples and responses to questions.");
        question.add("Ability to deliver the topic in a clear, organized, and logical manner.");
        question.add("Ability to engage the audience that fosters participation, enthusiasm, and message retention.");
        question.add("Relevance of the topic to my professional needs in my line of work and industry.");
        question.add("Appropriateness of the seminar's length (time) for the content covered.");
        question.add("Effectiveness of training method used (e.g. multimedia, presentation, case studies, activities, skills practice, etc.");
        question.add("Clarity and user friendliness of the hand-outs and/or participant materials.");
        question.add("Overall conduct of the seminar.");
        question.add("Overall seminar management and administration (facilities, venue, marketing, registration, customer service, food.");
        question.add("Overall rating for this seminar..");
        question.add("What areas(topic, concepts, or skills) made a positive impact on you?");
        question.add("What topics would you like to have been covered in more depth? What topics would you like us to cover in upcoming seminars?");
        question.add("Are there any other areas you would like to comment on to help us improved the effectiveness of this seminar (e.g. duration, " + "time in your career, quality of materials, topics, method of delivery, physical environment, use of pre-work)?");
        answers.add(0, "");

        items = question.size() - 1;
        tvCountQuestion.setText("1/"+ items);
        tvQuestion.setText(question.get(incrementQuestion));

        etAnswer.addTextChangedListener(new TextWatcher(){

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                answers.add(incrementQuestion, s.toString());
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{

                    if(validateAnswer()) {
                        if (incrementQuestion < 14) {
                            incrementQuestion++;
                            tvQuestion.setText(question.get(incrementQuestion));
                            tvCountQuestion.setText(incrementQuestion + "/" + items);

                            if (incrementQuestion == items) {
                                btnNext.setVisibility(View.GONE);
                                btnSubmit.setVisibility(View.VISIBLE);
                            } else if (incrementQuestion >= 11) {
                                rgAnswer.setVisibility(View.GONE);
                                btnSubmit.setVisibility(View.GONE);
                                btnNext.setVisibility(View.VISIBLE);
                                etAnswer.setVisibility(View.VISIBLE);
                            } else {
                                etAnswer.setVisibility(View.GONE);
                                btnSubmit.setVisibility(View.GONE);
                                btnNext.setVisibility(View.VISIBLE);
                                rgAnswer.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                    unSelectAnswer();

                }catch (Exception err){
                    Debugger.logD("Exception err "+err);
                }

            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{

                    if (incrementQuestion > 1) {
                        incrementQuestion--;
                        tvQuestion.setText(question.get(incrementQuestion));
                        tvCountQuestion.setText(incrementQuestion + "/" + items);

                        if (incrementQuestion == items) {
                            btnNext.setVisibility(View.GONE);
                            btnSubmit.setVisibility(View.VISIBLE);
                        } else if (incrementQuestion >= 11) {
                            rgAnswer.setVisibility(View.GONE);
                            btnSubmit.setVisibility(View.GONE);
                            btnNext.setVisibility(View.VISIBLE);
                            etAnswer.setVisibility(View.VISIBLE);
                        } else {
                            etAnswer.setVisibility(View.GONE);
                            btnSubmit.setVisibility(View.GONE);
                            btnNext.setVisibility(View.VISIBLE);
                            rgAnswer.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Toasty.warning(context, "No more question", Toast.LENGTH_SHORT).show();
                    }

                    unSelectAnswer();

                }catch (Exception err){
                    Debugger.logD("Exception err "+err);
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utility.haveNetworkConnection(context, getSupportFragmentManager())){
                    updateProfile();
                }
            }
        });

        rbExcellent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answers.add(incrementQuestion, "Excellent");
            }
        });

        rbVerySatisfactory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answers.add(incrementQuestion, "Very Satisfactory");
            }
        });

        rbSatisfactory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answers.add(incrementQuestion, "Satisfactory");
            }
        });

        rbFair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answers.add(incrementQuestion, "Fair");
            }
        });

        rbImprovement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answers.add(incrementQuestion, "Needs Improvement");
            }
        });


    }

    private void unSelectAnswer(){

        etAnswer.setText("");
        rgAnswer.clearCheck();
    }

    private boolean validateAnswer(){

        if(!rbExcellent.isChecked() && !rbVerySatisfactory.isChecked() && !rbSatisfactory.isChecked() && !rbFair.isChecked() && !rbImprovement.isChecked() && incrementQuestion < 11 ){
            Toasty.warning(context, context.getResources().getString(R.string.select_answer), Toast.LENGTH_LONG).show();
            return false;
        }else {
            return true;
        }

    }

    private void updateProfile()
    {
        try
        {
            progressDialog = new ProgressDialog(this);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("Token", UserSession.getToken(context));
            jsonObject.put("speakerID", speakers.getSpeakerID());
            jsonObject.put("seminarID", evaluation.getId());
            jsonObject.put("answer1", answers.get(1));
            jsonObject.put("answer2", answers.get(2));
            jsonObject.put("answer3", answers.get(3));
            jsonObject.put("answer4", answers.get(4));
            jsonObject.put("answer5", answers.get(5));
            jsonObject.put("answer6", answers.get(6));
            jsonObject.put("answer7", answers.get(7));
            jsonObject.put("answer8", answers.get(8));
            jsonObject.put("answer9", answers.get(9));
            jsonObject.put("answer10", answers.get(10));
            jsonObject.put("answer11", answers.get(11));
            jsonObject.put("answer12", answers.get(12));
            jsonObject.put("answer13", answers.get(13));

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            Debugger.logD(jsonObject.toString());

            HttpProvider.post(context, "post/saveevaluation", stringEntity, "application/json", new JsonHttpResponseHandler(){

                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog.show();
                    progressDialog.setMessage("Submitting evaluation...");

                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    progressDialog.hide();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Debugger.logD(response.toString());
                    Toasty.success(context, "Evaluation successfully submitted", Toast.LENGTH_LONG).show();
                    finish();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    progressDialog.hide();
                    Debugger.logD("Error From Server 3  1st");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Debugger.logD("Error From Server 2 ");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Debugger.logD("Error From Server 3 last ");
                }

            });

        } catch (Exception err)
        {
            Toasty.warning(context,"Error " + err.toString(),Toast.LENGTH_SHORT).show();
        }
    }



}
