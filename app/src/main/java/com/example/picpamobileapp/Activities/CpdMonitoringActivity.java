package com.example.picpamobileapp.Activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.example.picpamobileapp.MainActivity;
import com.example.picpamobileapp.Models.CpdMonitoring;
import com.example.picpamobileapp.Models.CpdMonitorings;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.DatePickers;
import com.example.picpamobileapp.Utils.DateTimeHandler;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.HttpProvider;
import com.example.picpamobileapp.Utils.Syncer;
import com.example.picpamobileapp.Utils.UserSession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;
import io.realm.Realm;

public class CpdMonitoringActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,TimePicker.OnTimeChangedListener{
    private Context context;
    private Button btnSubmit;
    private EditText etSeminarTopic, etDate, etTimeStart, etTimeEnd, etSeminarVenue, etSeminarSpeaker, etCertificate, etSeminarUnits;
    private Spinner spinnerCompetency;

    private boolean isDateStartSelected;
    private boolean isTimeSartSelected;
    public static String formattedTime;

    private Realm realm;

    private CpdMonitorings cpdMonitorings = new CpdMonitorings();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpd_monitoring);
        setTitle("CPD Monitoring");
        realm = Realm.getDefaultInstance();
        context = this;

        initializeUI();
        
    }

    private void initializeUI() {

        btnSubmit = (Button)findViewById(R.id.btnSubmit);
        etSeminarTopic = (EditText) findViewById(R.id.etSeminarTopic);
        etDate = (EditText)findViewById(R.id.etDate);
        etTimeStart = (EditText)findViewById(R.id.etTimeStart);
        etTimeEnd = (EditText)findViewById(R.id.etTimeEnd);
        etSeminarVenue = (EditText)findViewById(R.id.etSeminarVenue);
        etSeminarSpeaker = (EditText)findViewById(R.id.etSeminarSpeaker);
        etCertificate = (EditText)findViewById(R.id.etCertificate);
        spinnerCompetency = findViewById(R.id.spinnerCompetency);
        etSeminarUnits = (EditText)findViewById(R.id.etSeminarUnits);



        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isDateStartSelected = true;
                DatePickers.DatePickerForActivity fragment = new DatePickers.DatePickerForActivity();
                fragment.show(getSupportFragmentManager(), "date");
            }
        });
        etDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    isDateStartSelected = true;
                    DatePickers.DatePickerForActivity fragment = new DatePickers.DatePickerForActivity();
                    fragment.show(getSupportFragmentManager(), "date");
                }
            }
        });
        etTimeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTimeSartSelected = true;
                final Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CpdMonitoringActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String time = selectedHour + ":" + selectedMinute;
                        SimpleDateFormat fmt = new SimpleDateFormat("hh:mm");
                        Date date = null;
                        try {
                            date = fmt.parse(time);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");
                        formattedTime = fmtOut.format(date);
                        etTimeStart.setText(formattedTime);
                        //getDuration();
                    }
                }, hour, minute, false);//Yes 12 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        etTimeStart.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    isTimeSartSelected = true;
                    //setTime();
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(CpdMonitoringActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String time = selectedHour + ":" + selectedMinute;
                            SimpleDateFormat fmt = new SimpleDateFormat("hh:mm");
                            Date date = null;
                            try {
                                date = fmt.parse(time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");
                            formattedTime = fmtOut.format(date);
                            etTimeStart.setText(formattedTime);
                            //getDuration();
                        }
                    }, hour, minute, false);//Yes 12 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            }
        });
        etTimeEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTimeSartSelected = true;
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CpdMonitoringActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String time = selectedHour + ":" + selectedMinute;
                        SimpleDateFormat fmt = new SimpleDateFormat("hh:mm");
                        Date date = null;
                        try {
                            date = fmt.parse(time);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm a");
                        formattedTime = fmtOut.format(date);
                        etTimeEnd.setText(formattedTime);
                        //getDuration();
                    }
                }, hour, minute, false);//Yes 12 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        etTimeEnd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    isTimeSartSelected = true;
                    //setTime();
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(CpdMonitoringActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String time = selectedHour + ":" + selectedMinute;
                            SimpleDateFormat fmt = new SimpleDateFormat("hh:mm");
                            Date date = null;
                            try {
                                date = fmt.parse(time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm a");
                            formattedTime = fmtOut.format(date);
                            etTimeEnd.setText(formattedTime);
                            //getDuration();
                        }
                    }, hour, minute, false);//Yes 12 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //saveToDB();
                userAddseminar();
                saveToDB();
                finish();
                //Toasty.warning(context,"This Function is still unavailable").show();

            }

        });


    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        calendar.add(Calendar.DATE, 30);
        etDate.setText(DateTimeHandler.convertDatetoDisplayDate(date));
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
        String currentTime = sdfs.format(new Date());
        etTimeStart.setText(currentTime);
        etTimeEnd.setText(currentTime);
    }

    private void userAddseminar(){
        try
        {
            JSONObject jsonObject = new JSONObject();
            final ProgressDialog progressDialog = new ProgressDialog(context);
            jsonObject.put("Token", UserSession.getToken(context));
            jsonObject.put("seminar_topic", etSeminarTopic.getText().toString());
            jsonObject.put("seminar_speaker", etSeminarSpeaker.getText().toString());
            jsonObject.put("seminar_date", etDate.getText().toString());
            jsonObject.put("time_from", etTimeStart.getText().toString());
            jsonObject.put("time_to", etTimeEnd.getText().toString());
            jsonObject.put("certificate", etCertificate.getText().toString());
            jsonObject.put("competency", spinnerCompetency.getSelectedItemPosition() + 1);
            jsonObject.put("seminar_units", etSeminarUnits.getText().toString());
            jsonObject.put("seminar_venue", etSeminarVenue.getText().toString());


            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpProvider.post(context, "post/useraddseminar", stringEntity, "application/json", new JsonHttpResponseHandler(){

                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog.show();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    //progressDialog.dismiss();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Debugger.logD(response.toString());
                    Toasty.success(context, "Success").show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    Debugger.logD(response.toString());
                    //Toasty.success(context, "Success").show();
                    // Toast.makeText(getContext(),"Success",Toast.LENGTH_SHORT).show();
                    Toasty.success(getApplicationContext(),"Success  ");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    //Toasty.error(context, "Error From Server 1").show();
                    //Toast.makeText(getContext(),"Error From Server 1",Toast.LENGTH_SHORT).show();
                    Toasty.error(getApplicationContext(),"Error From Server 3 ");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    //Toasty.error(context, "Error from Server 2").show();
                    // Toast.makeText(getContext(),"Error From Server 2",Toast.LENGTH_SHORT).show();
                    Toasty.error(getApplicationContext(),"Error From Server 2 ");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    //Toasty.error(context, responseString).show();
                    //Toast.makeText(getContext(),"Error From Server 3 " + responseString,Toast.LENGTH_SHORT).show();
                    Toasty.error(getApplicationContext(),"Error From Server 3 ");
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode, headers, responseString);
                }
            });

        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
            //Toast.makeText(getContext(),"Error " + err.toString(),Toast.LENGTH_SHORT).show();
        }
    }

    private void saveToDB() {
            try {
                CpdMonitorings cpdMonitorings = null;

                if (this.cpdMonitorings.getUuid() != null) {
                    cpdMonitorings = realm.where(CpdMonitorings.class)
                            .equalTo("uuid", this.cpdMonitorings.getUuid())
                            .findFirst();
                }
                realm.beginTransaction();
                if (cpdMonitorings == null) {
                    CpdMonitorings cpdMonitorings1 = realm.createObject(CpdMonitorings.class, UUID.randomUUID().toString());
                    setFields(cpdMonitorings1);
                } else {
                    setFields(cpdMonitorings);
                }
                realm.commitTransaction();

                Toasty.success(context, "New Seminar Saved").show();
                finish();

            } catch (Exception err) {

                Toasty.warning(context, "Please complete the fields" + err).show();
            }
    }
    private void setFields(CpdMonitorings cpdMonitorings)
    {
        cpdMonitorings.setTopic(etSeminarTopic.getText().toString());
        cpdMonitorings.setUser_id(this.cpdMonitorings.getUser_id());
        cpdMonitorings.setDate(this.cpdMonitorings.getDate());
        cpdMonitorings.setTimeFrom(etTimeStart.getText().toString());
        cpdMonitorings.setTimeto(etTimeEnd.getText().toString());
        cpdMonitorings.setVenue(etSeminarVenue.getText().toString());
        cpdMonitorings.setSeminarSpeaker(etSeminarSpeaker.getText().toString());
        cpdMonitorings.setCertificate(etCertificate.getText().toString());
        cpdMonitorings.setSeminarUnits(etSeminarUnits.getText().toString());
        cpdMonitorings.setCompetency(spinnerCompetency.getSelectedItemPosition() + 1);

        Debugger.logD("1 " + etSeminarTopic.getText().toString());
        Debugger.logD("2 " + this.cpdMonitorings.getDate());
        Debugger.logD("3 " + etTimeStart.getText().toString());
        Debugger.logD("4 " + etTimeEnd.getText().toString());
        Debugger.logD("5 " + etSeminarVenue.getText().toString());
        Debugger.logD("6 " + etSeminarSpeaker.getText().toString());
        Debugger.logD("7 " + etCertificate.getText().toString());
        Debugger.logD("8 " + etSeminarUnits.getText().toString());
        Debugger.logD("9 " + spinnerCompetency.getSelectedItemPosition() + 1);
    }
}
