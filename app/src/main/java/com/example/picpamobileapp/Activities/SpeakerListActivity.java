package com.example.picpamobileapp.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.picpamobileapp.Adapters.SpeakerAdapter;
import com.example.picpamobileapp.Models.Evaluation;
import com.example.picpamobileapp.Models.Speakers;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.HttpProvider;
import com.example.picpamobileapp.Utils.UserSession;
import com.example.picpamobileapp.Utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

public class SpeakerListActivity extends AppCompatActivity implements SpeakerAdapter.ItemClickListener, SwipeRefreshLayout.OnRefreshListener{

    //Context
    private Context context;

    //Adapter
    private SpeakerAdapter speakerAdapter;

    //Widgets
    private View emptyIndicator;
    private TextView tvNoRecords;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeLayout;

    //Data
    private Realm realm;
    private Evaluation evaluation;
    private Speakers selectedSpeaker;
    private String errorMessage = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_speaker_list);
        context = this;
        realm = Realm.getDefaultInstance();

        evaluation = getIntent().getParcelableExtra("SEMINAR_OBJECT");

        setTitle("Select Speaker");
        initializeUI();
        syncData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        syncData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        final MenuItem searchViewItem = menu.findItem(R.id.action_search);

        final SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String searchValue) {
                readRecords( searchValue);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String searchValue) {
                readRecords( searchValue);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void initializeUI(){

        recyclerView = findViewById(R.id.rc_speaker);
        tvNoRecords = findViewById(R.id.tv_no_records);
        swipeLayout = findViewById(R.id.swipe_container);
        emptyIndicator = findViewById(R.id.viewEmptyListIndicator);

        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorAccent), getResources().getColor(android.R.color.holo_red_dark), getResources().getColor(android.R.color.holo_blue_dark), getResources().getColor(android.R.color.holo_orange_dark));
        swipeLayout.setProgressViewOffset(false,
                getResources().getDimensionPixelSize(R.dimen.refresher_offset),
                getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));

        readRecords("");
    }

    private void syncData(){

        if(Utility.haveNetworkConnection(context)) {
            syncSpeakers();
        } else{
            swipeLayout.setRefreshing(false);
            Toasty.warning(context, context.getResources().getString(R.string.connect_to_internet), Toast.LENGTH_SHORT).show();
        }

    }

    private void readRecords(String searchValue){

        RealmResults<Speakers> realmResults= realm.where(Speakers.class).contains("seminarspeaker", searchValue, Case.INSENSITIVE).findAll();
        realmResults.load();

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        if(realmResults.isLoaded()){
            speakerAdapter = new SpeakerAdapter(context, realmResults);
            speakerAdapter.setClickListener(this);
            recyclerView.setAdapter(speakerAdapter);

            showEmptyListIndicator(realmResults.size() == 0);
        }

    }

    @Override
    public void onItemClick(View view, int position) {
        selectedSpeaker = speakerAdapter.getItem(position);

        Intent intent = new Intent();
        intent.setClass(context, EvaluationActivity.class);
        intent.putExtra("SEMINAR_OBJECT", evaluation);
        intent.putExtra("SPEAKER_OBJECT", selectedSpeaker);
        startActivity(intent);
    }

    private void showEmptyListIndicator(boolean show) {

        emptyIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        tvNoRecords.setText(context.getResources().getString(R.string.all_done));

    }

    private void syncSpeakers(){

        try {

            JSONObject jsonParams = new JSONObject();

            jsonParams.put("Token", UserSession.getToken(context));
            jsonParams.put("seminarID", evaluation.getId());

            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/pendingspeakers", entity, true, new JsonHttpResponseHandler(){

                @Override
                public void onStart() {
                    super.onStart();
                    swipeLayout.setRefreshing(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try
                    {
                        Speakers speakers1 = new Speakers();
                        speakers1.delete();

                        ArrayList<Speakers> speakersArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<Speakers>>(){}.getType());

                        for (Speakers speakers : speakersArrayList)
                        {
                            speakers.save(speakers);
                        }

                        readRecords("");

                    } catch (Exception err)
                    {
                        errorMessage = err.toString();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    errorMessage = context.getResources().getString(R.string.server_error);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    errorMessage = context.getResources().getString(R.string.server_error);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    errorMessage = context.getResources().getString(R.string.server_error);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    swipeLayout.setRefreshing(false);

                    if(!errorMessage.equals("")){
                        Toasty.warning(context , errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        catch (Exception err)
        {
            Toasty.warning(context , err.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRefresh() {
        syncData();
    }
}
