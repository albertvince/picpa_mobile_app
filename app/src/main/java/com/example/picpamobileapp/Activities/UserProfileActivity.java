package com.example.picpamobileapp.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.picpamobileapp.Adapters.ProfilePagerAdapter;
import com.example.picpamobileapp.Fragments.ProfileCompanyFragment;
import com.example.picpamobileapp.Fragments.ProfileContactDetailsFragment;
import com.example.picpamobileapp.Fragments.ProfilePRCLicenseFragment;
import com.example.picpamobileapp.Fragments.ProfileUserDetailsFragment;
import com.example.picpamobileapp.Models.UserProfile;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.Syncer;

import java.io.File;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;

public class UserProfileActivity extends AppCompatActivity {

    //Context
    private Context context;

    //Data
    private Realm realm;
    private String imagePath = "";
    private UserProfile userProfile;
    private final int CODE_GALLERY_REQUEST = 999;
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 1;

    //Adapter
    private ViewPager viewPager;
    private ProfilePagerAdapter profilePagerAdapter;

    //Widgets
    private TabLayout tabLayout;
    private ImageView imgProfile;
    private EditText etFirstName;
    private ProgressDialog progressDialog;
    private TextView tvName, tvCPA, tvOpenGallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile2);
        realm = Realm.getDefaultInstance();
        context = getApplicationContext();

        userProfile = realm.where(UserProfile.class).findFirst();

        if (userProfile == null) {
            Syncer.syncUserProfile(this, false, this);
        } else {
            initializeUI();
        }

        this.setTitle("Edit Profile");

        verifyStoragePermissions(context);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public void initializeUI() {
        progressDialog = new ProgressDialog(this);
        etFirstName = findViewById(R.id.etFname);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewpager);

        profilePagerAdapter = new ProfilePagerAdapter(getSupportFragmentManager());

        profilePagerAdapter.addFragment(ProfileUserDetailsFragment.newInstance(), "User Details");
        profilePagerAdapter.addFragment(ProfileContactDetailsFragment.newInstance(), "Contact Details");
        profilePagerAdapter.addFragment(ProfilePRCLicenseFragment.newInstance(), "PRC License");
        profilePagerAdapter.addFragment(ProfileCompanyFragment.newInstance(), "Company");

        viewPager.setAdapter(profilePagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        imgProfile = findViewById(R.id.img_profile);
        tvOpenGallery = findViewById(R.id.tv_open_gallery);

        tvName = findViewById(R.id.tvName);
        tvCPA = findViewById(R.id.tvCPA);

        userProfile = realm.where(UserProfile.class).findFirst();

        tvName.setText(userProfile.getFirst_name() + " " + userProfile.getLast_name() + " " + userProfile.getSuffixes());

        if (userProfile.getUser_image() != null) {
            displayImage(userProfile.getUser_image());
        }

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPicture();
            }
        });

        tvOpenGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPicture();
            }
        });


        if (userProfile.getRegister_no().isEmpty()) {
            tvCPA.setText("Non-CPA");
        } else {
            tvCPA.setText("CPA");
        }
    }

    public void verifyStoragePermissions(final Context context) {

        try {

            int permissionCheckStorage = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int permissionCheckCamera = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);

            if (permissionCheckStorage != PackageManager.PERMISSION_GRANTED && permissionCheckCamera != PackageManager.PERMISSION_GRANTED) {

                // if storage request is denied
                if (ActivityCompat.shouldShowRequestPermissionRationale(UserProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("You need to give permission to access storage in order to work this feature.");

                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

                    builder.setPositiveButton("GIVE PERMISSION", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                            // Show permission request popup
                            ActivityCompat.requestPermissions(UserProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, STORAGE_PERMISSION_REQUEST_CODE);
                        }
                    });
                    builder.show();

                } //asking permission for first time
                else {
                    // Show permission request popup for the first time
                    ActivityCompat.requestPermissions(UserProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, STORAGE_PERMISSION_REQUEST_CODE);

                }

            }
        } catch (Exception err) {
            Debugger.logD("verifyStoragePermissions Exception error: " + err);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CODE_GALLERY_REQUEST && resultCode == RESULT_OK && data != null) {
            try {
                Uri selectedImage = data.getData();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = context.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                userProfile = realm.where(com.example.picpamobileapp.Models.UserProfile.class).findFirst();

                updateUserProfile(userProfile.getId(), picturePath);

                displayImage(userProfile.getUser_image());

                imagePath = picturePath;

                Toasty.success(context, "Photo Selected", Toast.LENGTH_SHORT).show();
                tvOpenGallery.setVisibility(View.GONE);

            } catch (Exception e) {
                Debugger.logD("onActivityResult Exception " + e);
            }
        }
    }

    //Display list of image using Glide
    private void displayImage(String imagePath) {
        RequestOptions myOption = new RequestOptions().circleCrop();

        Glide.with(context).load(imagePath).apply(myOption).into(imgProfile);

    }

    private void updateUserProfile(final int id, final String imagePath) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                UserProfile userProfile = realm.where(UserProfile.class).equalTo("id", id).findFirst();
                userProfile.setUser_image(imagePath);
            }
        });
    }

    public static String getName(String filePath) {
        if (filePath == null || filePath.length() == 0) {
            return "";
        }
        int extract = filePath.lastIndexOf('?');
        if (extract > 0) {
            filePath = filePath.substring(0, extract);
        }
        int namePos = filePath.lastIndexOf(File.separatorChar);
        return (namePos >= 0) ? filePath.substring(namePos + 1) : filePath;
    }

    public void addPicture() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, CODE_GALLERY_REQUEST);
    }

}
