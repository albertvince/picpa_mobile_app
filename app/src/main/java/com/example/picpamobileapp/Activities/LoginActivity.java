package com.example.picpamobileapp.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.picpamobileapp.Adapters.LoginPageAdapter;
import com.example.picpamobileapp.Adapters.ProfilePagerAdapter;
import com.example.picpamobileapp.Fragments.LoginSignInFragment;
import com.example.picpamobileapp.Fragments.LoginSignUpFragment;
import com.example.picpamobileapp.Fragments.ProfileCompanyFragment;
import com.example.picpamobileapp.Fragments.ProfileContactDetailsFragment;
import com.example.picpamobileapp.Fragments.ProfilePRCLicenseFragment;
import com.example.picpamobileapp.Fragments.ProfileUserDetailsFragment;
import com.example.picpamobileapp.MainActivity;
import com.example.picpamobileapp.R;

public class LoginActivity extends AppCompatActivity {
    private Context context;
    private ViewPager viewPager;
    private LoginPageAdapter loginPageAdapter;
    private TabLayout tabLayout;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = getApplicationContext();
        initializeUI();
    }

    private void initializeUI(){
        tabLayout = findViewById(R.id.tabLayoutLogin);
        viewPager = findViewById(R.id.vpLogin);
        loginPageAdapter = new LoginPageAdapter(getSupportFragmentManager());
        loginPageAdapter.addFragment(LoginSignInFragment.newInstance(), "Sign In");
        loginPageAdapter.addFragment(LoginSignUpFragment.newInstance(), "Sign Up");
        viewPager.setAdapter(loginPageAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
