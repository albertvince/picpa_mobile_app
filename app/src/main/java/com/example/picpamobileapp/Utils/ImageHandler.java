package com.example.picpamobileapp.Utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class ImageHandler {

    public static boolean fileExists(Context context, String fileName)
    {
        ContextWrapper contextWrapper = new ContextWrapper(context);

        File myImagesDirectory = contextWrapper.getDir("my_images", 0);
        File subDirectory = new File(myImagesDirectory, "3sImage");

        if (!subDirectory.exists()) return false;

        File file = new File(subDirectory, fileName);
        return file.exists();
    }

    public static byte[] getImageByPath(String path) throws Exception
    {
        try
        {
            Bitmap bm = BitmapFactory.decodeFile(path);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            return baos.toByteArray();
        }
        catch (Exception err)
        {
            throw new Exception("Error getting image: \n" + err.toString());
        }
    }

}
