package com.example.picpamobileapp.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Debug;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.example.picpamobileapp.Fragments.CpdMonitoringFragment;
import com.example.picpamobileapp.Fragments.EvaluationFragment;
import com.example.picpamobileapp.Fragments.UpseminarsFragment;
import com.example.picpamobileapp.Models.Chapter;
import com.example.picpamobileapp.Models.Competencies;
import com.example.picpamobileapp.Models.CpdMonitoring;
import com.example.picpamobileapp.Models.CpdMonitorings;
import com.example.picpamobileapp.Models.Evaluation;
import com.example.picpamobileapp.Models.OngoingImages;
import com.example.picpamobileapp.Models.Speakers;
import com.example.picpamobileapp.Models.SubTopics;
import com.example.picpamobileapp.Models.UpcomingImages;
import com.example.picpamobileapp.Models.UpcomingSeminar;
import com.example.picpamobileapp.Models.UserProfile;
import com.example.picpamobileapp.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.Base64;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

public class Syncer  {

    public static void syncCpdMonitoring(final Context context)
    {

        final ProgressDialog progressDialog = new ProgressDialog(context);
        try {
            progressDialog.show();
            progressDialog.setMessage("Loading CPD Monitoring");
            if(!Utility.haveNetworkConnection(context)){
                progressDialog.dismiss();
                Toasty.error(context, "Internet connection is required").show();
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/cpdmonitoringapi", entity, true, new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.dismiss();
                   try {
                       if (response != null){
                           CpdMonitoring cpdMonitoring1 = new CpdMonitoring();
                           cpdMonitoring1.delete();

                           ArrayList<CpdMonitoring> cpdMonitoringArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<CpdMonitoring>>(){}.getType());
                           for (CpdMonitoring cpdMonitoring : cpdMonitoringArrayList)
                           {
                               cpdMonitoring.save(cpdMonitoring);
                           }
//                           ((CpdMonitoringFragment)fragment).readRecords("");
                       }
                   }
                   catch (Exception err)
                   {
                       Debugger.logD(err.toString());
                   }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();

                    Debugger.printO("Response String " + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();

                    Debugger.printO("Json Array " + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context, "An error occured while syncing").show();

                    Debugger.printO("Json Object " + errorResponse);
                }
            });
        }
        catch (Exception err)
        {
            Toast.makeText(context,"Error " + err.toString(),Toast.LENGTH_SHORT).show();
        }
    }

    public static void syncCpdMonitoringSubTopics(final Context context, final Fragment fragment)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        try {
            progressDialog.show();
            progressDialog.setMessage("Loading Subtopics");
            if(!Utility.haveNetworkConnection(context)){
                progressDialog.dismiss();
                Toasty.error(context, "Internet connection is required").show();
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/subtopicseminar", entity, true, new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        if (response != null){
                            SubTopics subTopics1 = new SubTopics();
                            subTopics1.delete();

                            ArrayList<SubTopics> subTopicsArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<SubTopics>>(){}.getType());
                            for (SubTopics subTopics : subTopicsArrayList)
                            {
                                subTopics.save(subTopics);
                            }
                            ((CpdMonitoringFragment)fragment).loadSubTopics();
                        }
                    }
                    catch (Exception err)
                    {
                        Debugger.logD(err.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();

                    Debugger.printO("Response String " + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();

                    Debugger.printO("Json Array " + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context, "An error occured while syncing").show();
                    Debugger.printO("Json Object " + errorResponse);
                }
            });
        }
        catch (Exception err)
        {
            Toasty.warning(context, "An error occured while syncing").show();
        }
    }

    public static void syncCpdMonitoringCompetencies(final Context context)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        try {
            progressDialog.show();
            progressDialog.setMessage("Loading competencies");
            if(!Utility.haveNetworkConnection(context)){
                progressDialog.dismiss();
                Toasty.error(context, "Internet connection is required").show();
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/userscompetency", entity, true, new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        if (response != null){
                            Competencies competencies1 = new Competencies();
                            competencies1.delete();

                            ArrayList<Competencies> competenciesArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<Competencies>>(){}.getType());

                            for (Competencies competencies : competenciesArrayList)
                            {
                                competencies.save(competencies);
                            }

//                            ((CpdMonitoringFragment)fragment).loadSubTopics();
                        }
                    }
                    catch (Exception err)
                    {
                        Debugger.logD(err.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();

                    Debugger.printO("Response String " + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();

                    Debugger.printO("Json Array " + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context, "An error occured while syncing").show();
                    Debugger.printO("Json Object " + errorResponse);
                }
            });
        }
        catch (Exception err)
        {
            Toasty.warning(context, "An error occured while syncing").show();
        }
    }

    public static void syncCpdMonitorings(final Context context)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        try{
            progressDialog.show();
            progressDialog.setMessage("Loading CPD Monitoring");
            if(!Utility.haveNetworkConnection(context)){
                progressDialog.dismiss();
                Toasty.error(context, "Internet connection is required").show();
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/userseminarapi", entity, true, new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        if (response.getJSONArray("result") != null){
                            CpdMonitorings cpdMonitorings1 = new CpdMonitorings();
                            cpdMonitorings1.delete();

                            ArrayList<CpdMonitorings> cpdMonitoringArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<CpdMonitorings>>(){}.getType());

                            for (CpdMonitorings cpdMonitorings : cpdMonitoringArrayList)
                            {
                                cpdMonitorings.save(cpdMonitorings);
                            }
//                            ((CpdMonitoringFragment)fragment).readRecords1("");
                        }
                    }
                    catch (Exception err)
                    {
                        Debugger.logD(err.toString());
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();

                    Debugger.printO("Response String " + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();

                    Debugger.printO("Json Array " + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();

                    Debugger.printO("Json Object " + errorResponse);
                }
            });
        }
        catch (Exception err)
        {
            Toasty.warning(context , "An error occured while syncing").show();
        }
    }

    public static void syncEvaluation(final Context context, final Fragment fragment)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        try{
            progressDialog.show();
            progressDialog.setMessage("Loading Evaluation");
            if(!Utility.haveNetworkConnection(context)){
                progressDialog.dismiss();
                Toasty.error(context, "No internet connection detected").show();
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/pendingevaluation", entity, true, new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        if (response != null){
                            Evaluation evaluation1 = new Evaluation();
                            evaluation1.delete();

                            ArrayList<Evaluation> evaluationArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<Evaluation>>(){}.getType());

                            for (Evaluation evaluation : evaluationArrayList)
                            {
                                evaluation.save(evaluation);
                            }
                            ((EvaluationFragment)fragment).initializeUI();
                        }
                    }
                    catch (Exception err)
                    {
                        Toasty.warning(context , "An error occured while syncing").show();

                        Debugger.printError(err);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();

                    Debugger.printO("Response String " + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();
                    Debugger.printO("Json Array " + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();
                    Debugger.printO("Json Object " + errorResponse);
                }
            });
        }
        catch (Exception err)
        {
            Toasty.warning(context , "An error occured while syncing").show();
        }

    }

    public static void syncSpeakers(final Context context, final boolean notify, final Activity activity, String seminarID){
        final ProgressDialog progressDialog = new ProgressDialog(context);

        try {
            progressDialog.show();
            progressDialog.setMessage("Loading speakers");
            if(!Utility.haveNetworkConnection(context)){
                throw new Exception("Internet connection is required");
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            jsonParams.put("seminarID", seminarID);
            StringEntity entity = new StringEntity(jsonParams.toString());

            Debugger.printO(seminarID + UserSession.getToken(context));

            HttpProvider.post(context, "post/pendingspeakers", entity, true, new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.hide();
                    Debugger.printO(response.toString());
                    try {
                        if (response != null){
                            Speakers speakers1 = new Speakers();
                            speakers1.delete();
                            ArrayList<Speakers> speakersArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<Speakers>>(){}.getType());
                            for (Speakers speakers : speakersArrayList)
                            {
                                speakers.save(speakers);
                            }
                        }
                    }
                    catch (Exception err)
                    {
                        Debugger.logD(err.toString());
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();
                    Debugger.printO("Response String " + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();
                    Debugger.printO("Json Array " + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();
                    Debugger.printO("Json Object " + errorResponse);
                }
            });
        }
        catch (Exception err)
        {
            Toasty.warning(context , "An error occured while syncing").show();
        }
    }

    public static void syncUpcomingSeminar(final Context context, final Fragment fragment)
    {
        try{
            Toasty.warning(context, "Nag start na ug sync").show();
            final ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.show();
            progressDialog.setMessage("Loading speakers");
            progressDialog.setCancelable(false);

            if(!Utility.haveNetworkConnection(context)){
                throw new Exception("Internet connection is required");
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/upcomingseminars", entity, true, new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        if (response != null)
                        {
                            UpcomingSeminar upcomingSeminar1 = new UpcomingSeminar();
//                            upcomingSeminar1.delete();

                            ArrayList<UpcomingSeminar> upcomingSeminarArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<UpcomingSeminar>>(){}.getType());

//                            for (UpcomingSeminar upcomingSeminar : upcomingSeminarArrayList)
//                            {
//                                upcomingSeminar.save(upcomingSeminar);
//                            }
                            ((UpseminarsFragment)fragment).initializeUI();
                        }
                        else {
                            return;
                        }
                    }
                    catch (Exception err)
                    {
                        Toasty.warning(context , "An error occured while syncing").show();
                        Debugger.logD(err.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();
                    Debugger.printO("Response String " + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();
                    Debugger.printO("Json Array " + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();
                    Debugger.printO("Json Object " + errorResponse);
                }
            });
        }
        catch (Exception err)
        {
            Toasty.warning(context , "An error occured while syncing").show();
        }

    }
    public static void syncUpcomingImages(final Context context,final boolean notify)
    {
        try{
            if(!Utility.haveNetworkConnection(context)){
                throw new Exception("Internet connection is required");
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/upcomingimages", entity, true, new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        ArrayList<UpcomingImages> upcomingImagesArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<UpcomingImages>>(){}.getType());

                        UpcomingImages upcomingImages1 = new UpcomingImages();
                        upcomingImages1.delete();

                        for (UpcomingImages upcomingImages : upcomingImagesArrayList)
                        {
                            upcomingImages.save(upcomingImages);

                        }
                    }
                    catch (Exception err)
                    {
                        Debugger.logD(err.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (notify)
                    {
                        Toasters.HideLoadingSpinner();
                        Toasters.ShowToast(context, "Sync Failed 101 : " + responseString);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    if (notify) Toasters.HideLoadingSpinner();
                    String errorMessaage = errorResponse != null ? errorResponse.toString() : " Check Internet Connection";
                    Toasters.ShowToast(context, "Sync Failed 102 : \n" + errorMessaage);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (notify)
                    {
                        Toasters.HideLoadingSpinner();
                    }
                    Toasters.ShowToast(context, "Sync Failed 103 : ");
                }
            });
        }
        catch (Exception err)
        {

        }

    }
    public static void syncOngoingImages(final Context context,final boolean notify)
    {
        try{
            if(!Utility.haveNetworkConnection(context)){
                throw new Exception("Internet connection is required");
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/ongoingimages", entity, true, new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {

                        ArrayList<OngoingImages> ongoingImagesArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<OngoingImages>>(){}.getType());

                        OngoingImages ongoingImages1 = new OngoingImages();
                        ongoingImages1.delete();
                        for (OngoingImages ongoingImages : ongoingImagesArrayList)
                        {
                            ongoingImages.save(ongoingImages);

                        }
                    }
                    catch (Exception err)
                    {
                        Debugger.logD(err.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (notify)
                    {
                        Toasters.HideLoadingSpinner();
                        Toasters.ShowToast(context, "Sync Failed 101 : " + responseString);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    if (notify) Toasters.HideLoadingSpinner();
                    String errorMessaage = errorResponse != null ? errorResponse.toString() : " Check Internet Connection";
                    Toasters.ShowToast(context, "Sync Failed 102 : \n" + errorMessaage);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (notify)
                    {
                        Toasters.HideLoadingSpinner();
                    }
                    Toasters.ShowToast(context, "Sync Failed 103 : ");
                }
            });
        }
        catch (Exception err)
        {

        }

    }

    public static void syncUserProfile(final Context context, final boolean notify, final Activity activity)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        try{
            progressDialog.show();
            progressDialog.setMessage("Syncing User Profile");
            progressDialog.setCancelable(false);
            if(!Utility.haveNetworkConnection(context)){
                throw new Exception("Internet connection is required");
            }

            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/memberdetails", entity, true, new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.hide();
                    try {
                        if (response != null){
                            ArrayList<UserProfile> userProfileArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<UserProfile>>(){}.getType());
                            for (UserProfile userProfile : userProfileArrayList)
                            {
                                userProfile.save(userProfile, activity);
                            }
                        }
                        else {
                            return;
                        }
                    }
                    catch (Exception err)
                    {
                        Toasty.warning(context , "An error occured while syncing").show();
                        Debugger.logD(err.toString());
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();
                    Debugger.printO("Response String " + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();
                    Debugger.printO("Json Array " + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occured while syncing").show();
                    Debugger.printO("Json Object " + errorResponse);
                }
            });
        }
        catch (Exception err)
        {
            Toasty.warning(context , "An error occured while syncing").show();
        }

    }

    public static class UpdateChapter extends AsyncTask<String, Integer, String> {

        private Context context;
        private String success = "", errorMessage = "";
        private ProgressDialog progressDialog;

        public UpdateChapter(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("updating...");
            progressDialog.show();

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                JSONObject jsonParams = new JSONObject();

                jsonParams.put("Token", UserSession.getToken(context));
                StringEntity entity = new StringEntity(jsonParams.toString());

                HttpProvider.postSync(context, "post/chaptersapi", entity, true, new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {

                            Chapter.delete();
                            ArrayList<Chapter> chapterArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<Chapter>>() {}.getType());

                            for(Chapter chapter : chapterArrayList){
                                chapter.save(chapter);
                            }

                        } catch (Exception err) {
                            Debugger.logD("UpdateChapter error "+err.toString());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        errorMessage = context.getResources().getString(R.string.server_error);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        errorMessage = context.getResources().getString(R.string.server_error);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        errorMessage = context.getResources().getString(R.string.server_error);
                    }

                    @Override
                    public void onStart() {
                        super.onStart();
                        errorMessage = "";
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();

                        if(errorMessage.equals("")){
                            success += "";
                        }else {
                            success += "FAILED";
                        }
                    }

                });

            } catch (Exception err) {
                errorMessage = "doInBackground Exception " + err;
                Debugger.logD(errorMessage);
                return success += "FAILED";
            }

            return success;

        }

        @Override
        protected void onPostExecute(String success) {

            if (success.isEmpty()) {
//                Toasty.success(context, "Successfully update").show();
            } else {
                Toasty.warning(context, "Failed to update!").show();
            }

            progressDialog.dismiss();
            Debugger.logD("success "+success);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

    }

    public static class UpdateProfileDetails extends AsyncTask<String, Integer, String> {

        //Context
        private Context context;

        //Data
        private Realm realm;
        private boolean isUpdatePrc;
        private boolean isUpdateProfile;
        private UserProfile userProfile;

        //Widgets
        private ProgressDialog progressDialog;
        private String success = "", errorMessage = "";

        public UpdateProfileDetails(Context context, UserProfile userProfile,  boolean isUpdateProfile,  boolean isUpdatePrc) {
            this.context = context;
            this.userProfile = userProfile;
            this.isUpdatePrc = isUpdatePrc;
            this.isUpdateProfile = isUpdateProfile;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Please wait while updating...");
            progressDialog.show();

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                realm= Realm.getDefaultInstance();
                userProfile = realm.where(UserProfile.class).findFirst();
                assert userProfile != null;
                userProfile.load();

                if(userProfile.isLoaded()) {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("Token", UserSession.getToken(context));
                    jsonObject.put("chapter", userProfile.getChapter());
                    jsonObject.put("username", userProfile.getUsername());
                    jsonObject.put("password", userProfile.getPassword());
                    jsonObject.put("suffixes", userProfile.getSuffixes());
                    jsonObject.put("email_add", userProfile.getEmail_add());
                    jsonObject.put("last_name", userProfile.getLast_name());
                    jsonObject.put("first_name", userProfile.getFirst_name());
                    jsonObject.put("contact_no", userProfile.getContact_no());
                    jsonObject.put("valid_until", userProfile.getValid_until());
                    jsonObject.put("middle_name", userProfile.getMiddle_name());
                    jsonObject.put("register_no", userProfile.getRegister_no());
                    jsonObject.put("sector", userProfile.getSector() + "");
                    jsonObject.put("facebook_acc", userProfile.getFacebook_acc());
                    jsonObject.put("full_address", userProfile.getFull_address());
                    jsonObject.put("company_name", userProfile.getCompany_name());
                    jsonObject.put("register_date", userProfile.getRegister_date());
                    jsonObject.put("billing_address", userProfile.getBilling_address());

                    StringEntity stringEntity = new StringEntity(jsonObject.toString());

                    HttpProvider.postSync(context, "post/memberupdate", stringEntity, false, new JsonHttpResponseHandler() {

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                            errorMessage = context.getResources().getString(R.string.server_error);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                            errorMessage = context.getResources().getString(R.string.server_error);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            super.onFailure(statusCode, headers, responseString, throwable);
                            errorMessage = context.getResources().getString(R.string.server_error);
                        }

                        @Override
                        public void onStart() {
                            super.onStart();
                            errorMessage = "";
                        }

                        @Override
                        public void onFinish() {
                            super.onFinish();

                            if (errorMessage.equals("")) {
                                success += "";
                            } else {
                                success += "FAILED";
                            }
                        }

                    });

                    if (isUpdateProfile) {

                        String image = "";

                        if (userProfile.getUser_image() != null && userProfile.getUser_image().length() > 0 && userProfile.getUser_image().contains("storage")) {
                            Debugger.logD("getUser_image " + userProfile.getUser_image());

                            image = Base64.encodeToString(ImageHandler.getImageByPath(userProfile.getUser_image()), Base64.DEFAULT);

                            JSONObject profileJsonObject = new JSONObject();
                            profileJsonObject.put("Token", UserSession.getToken(context));
                            profileJsonObject.put("Image", image);

                            StringEntity profileStringEntity = new StringEntity(profileJsonObject.toString());

                            HttpProvider.postSync(context, "post/uploadprofilepic", profileStringEntity, true, new JsonHttpResponseHandler() {

                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    super.onSuccess(statusCode, headers, response);
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                    super.onFailure(statusCode, headers, throwable, errorResponse);
                                    errorMessage = context.getResources().getString(R.string.server_error);
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                                    super.onFailure(statusCode, headers, throwable, errorResponse);
                                    errorMessage = context.getResources().getString(R.string.server_error);
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                    super.onFailure(statusCode, headers, responseString, throwable);
                                    errorMessage = context.getResources().getString(R.string.server_error);
                                }

                                @Override
                                public void onStart() {
                                    super.onStart();
                                    errorMessage = "";
                                }

                                @Override
                                public void onFinish() {
                                    super.onFinish();

                                    if (errorMessage.equals("")) {
                                        success += "";
                                    } else {
                                        success += "FAILED";
                                    }
                                }

                            });
                        }

                    }
                    if (isUpdatePrc) {

                        String imagePrc = "";

                        Debugger.logD("getPrc_image2 " + userProfile.getPrc_image());
                        if (userProfile.getPrc_image() != null && userProfile.getPrc_image().contains("storage")) {
                            Debugger.logD("getPrc_image3 " + userProfile.getPrc_image());

                            imagePrc = Base64.encodeToString(ImageHandler.getImageByPath(userProfile.getPrc_image()), Base64.DEFAULT);

                            JSONObject prcJsonObject = new JSONObject();
                            prcJsonObject.put("Token", UserSession.getToken(context));
                            prcJsonObject.put("Image", imagePrc);

                            StringEntity prcStringEntity = new StringEntity(prcJsonObject.toString());

                            HttpProvider.postSync(context, "post/uploadprcimage", prcStringEntity, true, new AsyncHttpResponseHandler() {

                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    errorMessage = context.getResources().getString(R.string.server_error);
                                }

                                @Override
                                public void onStart() {
                                    super.onStart();
                                    errorMessage = "";
                                }

                                @Override
                                public void onFinish() {
                                    super.onFinish();

                                    if (errorMessage.equals("")) {
                                        success += "";
                                    } else {
                                        success += "FAILED";
                                    }
                                }

                            });
                        }
                    }
                }
            } catch (Exception err) {
                errorMessage = "doInBackground Exception " + err;
                Debugger.logD(errorMessage);
                return success += "FAILED";
            }

            return success;

        }

        @Override
        protected void onPostExecute(String success) {

            if (success.isEmpty()) {
                Toasty.success(context, "Successfully update").show();
            } else {
                Toasty.warning(context, "Failed to update!").show();
            }

            progressDialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

    }

}
