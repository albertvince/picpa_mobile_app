package com.example.picpamobileapp.Utils;

import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

public class Migration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();

        if (oldVersion == 1) {
            RealmObjectSchema taskSchema = schema.get("UpcomingSeminar");

            taskSchema.addField("isPaid", boolean.class)
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            obj.setBoolean("isPaid", false);
                        }
                    });

            taskSchema.addField("id", int.class)
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            obj.setInt("id", 0);
                        }
                    });

            taskSchema.addField("prc_image", String.class)
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            obj.setString("prc_image", "");
                        }
                    });

            oldVersion++;
        }

        if (oldVersion == 2) {
            schema.create("Chapter")
                    .addField("chapterId", int.class)
                    .addField("chapterName", String.class);

            schema.create("CpdMonitoring")
                    .addField("downloadPdf", String.class);
            oldVersion++;
        }

        Debugger.logD(" Migrate "+oldVersion);

    }

    @Override
    public int hashCode() {
        return 37;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Migration);
    }
}
