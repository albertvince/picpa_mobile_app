package com.example.picpamobileapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.picpamobileapp.Models.Evaluation;
import com.example.picpamobileapp.R;

import java.util.List;

import io.realm.RealmResults;


public class EvaluationAdapter extends RecyclerView.Adapter<EvaluationAdapter.ViewHolder> {

    private Context mContext;
    private List<Evaluation> mList;
    private ItemClickListener mClickListener;

    public EvaluationAdapter(@NonNull Context context, RealmResults<Evaluation> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_evaluation,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        final Evaluation evaluation = mList.get(i);

        TextView tvTopic, tvDate, tvTime;

        tvTopic = holder.tvTopic;
        tvDate = holder.tvDate;
        tvTime = holder.tvTime;

        tvTopic.setText( evaluation.getTopic());
        tvDate.setText( evaluation.getDate());
        tvTime.setText( evaluation.getTimeFrom() + " - " + evaluation.getTimeTo());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener, View.OnCreateContextMenuListener{
        TextView tvTopic, tvTime, tvDate;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvDate = itemView.findViewById(R.id.tvDate);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvTopic = itemView.findViewById(R.id.tvTopic);
            cardView = itemView.findViewById(R.id.cv_evaluation);

            cardView.setOnClickListener(this);
            cardView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }

    public Evaluation getItem(int id) {
        return mList.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


}