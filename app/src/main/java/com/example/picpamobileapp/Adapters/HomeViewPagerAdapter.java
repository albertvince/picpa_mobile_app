package com.example.picpamobileapp.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.picpamobileapp.Activities.DetailsActivity;
import com.example.picpamobileapp.Models.UpcomingImages;
import com.example.picpamobileapp.Models.UpcomingSeminar;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class HomeViewPagerAdapter extends PagerAdapter {

    private BottomSheetBehavior mBottomSheetBehavoir;

    private View view;

    private LayoutInflater layoutInflater;
    private ArrayList<UpcomingImages> images = new ArrayList<>();
    private Context mContext;
    private List<UpcomingImages> mList;
    private Fragment fragment;
//    private ArrayList<String> names = new ArrayList<>();
//    private ArrayList<Float> ratings = new ArrayList<>();

    public HomeViewPagerAdapter(Context context, List<UpcomingImages> list, Fragment fragment) {
        mContext = context;
        mList = list;
        this.fragment = fragment;
        //this.images = images;
//        this.names = names;
//        this.ratings = ratings;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View view = layoutInflater.inflate(R.layout.home_header_images, null);

//        Realm realm = Realm.getDefaultInstance();
        final UpcomingImages upcomingImages = mList.get(position);

        Debugger.logD(upcomingImages.getImage_name());
        ImageView imageView = view.findViewById(R.id.iv_HeaderImage);
//        TextView textView = view.findViewById(R.id.tv_HeaderName);
        //imageView.setText(upcomingImages.getImage_name());

                view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBottomSheet(v);
            }
                    private  void openBottomSheet(View v){
                        Context context=v.getContext();
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                        View view = inflater.inflate(R.layout.bottom_sheet, null);
                        TextView txtView = (TextView)view.findViewById(R.id.txView);


                        final Dialog mBottomSheetDialog = new Dialog (context);
                        Window window = mBottomSheetDialog.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();
                        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                        window.setAttributes(wlp);

                        mBottomSheetDialog.setContentView (view);
                        mBottomSheetDialog.setCancelable (true);
                        mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);
                        mBottomSheetDialog.getWindow ().setGravity (Gravity.BOTTOM);
                        mBottomSheetDialog.getWindow().getAttributes().windowAnimations = R.style.DialogUpAnimation;
                        //mBottomSheetDialog.getWindow().getAttributes().windowAnimations = R.style.DialogDownAnimation;
                        mBottomSheetDialog.show ();
                        //txtView.setText(" " + names.get(position));
                        txtView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                               // mBottomSheetDialog.getWindow ().setGravity (Gravity.TOP);
                                mBottomSheetDialog.getWindow().getAttributes().windowAnimations = R.style.DialogDownAnimation;
                                return true;
                            }
                        });

                    }
            });
        Debugger.logD("FUCKING FUCK! " + position);


        RequestOptions myOption = new RequestOptions()
                .centerInside();
        Glide.with(fragment)
                .load("https://picpacdomisor.org/images/carousel/"+upcomingImages.getImage_name())
                .apply(myOption)
                .into(imageView);

        //textView.setText(names.get(position));

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;
    }

}
