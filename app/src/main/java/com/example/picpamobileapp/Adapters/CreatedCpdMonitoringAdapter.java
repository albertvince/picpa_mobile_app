package com.example.picpamobileapp.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.picpamobileapp.Models.CpdMonitoring;
import com.example.picpamobileapp.Models.CpdMonitorings;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmResults;

public class CreatedCpdMonitoringAdapter extends RecyclerView.Adapter<CreatedCpdMonitoringAdapter.ViewHolder> {

    private Context mContext;
    private List<CpdMonitorings> mList =  new ArrayList<>();
    public CreatedCpdMonitoringAdapter(@NonNull Context context, RealmResults<CpdMonitorings> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_cpd_monitoring,viewGroup, false);

        CreatedCpdMonitoringAdapter.ViewHolder viewHolder = new CreatedCpdMonitoringAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {

        final CpdMonitorings cpdMonitorings = mList.get(i);


        TextView tvTopic, tvDate, tvVenue, tvTime;
        Button btnview;

        tvTopic = holder.tvTopic;
        tvDate = holder.tvDate;
        tvVenue = holder.tvVenue;
        tvTime = holder.tvTime;
        btnview = holder.btnView;



        tvTopic.setText(cpdMonitorings.getTopic());
        tvDate.setText(cpdMonitorings.getDate());
        tvVenue.setText(cpdMonitorings.getVenue());
        tvTime.setText(cpdMonitorings.getTimeFrom() + " - " + cpdMonitorings.getTimeTo());
        Debugger.printO(cpdMonitorings.getTimeFrom() + " - " + cpdMonitorings.getTimeTo());

        btnview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBottomSheet(v);
            }

            private  void openBottomSheet(View v){

                final Context context=v.getContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                View view = inflater.inflate(R.layout.cpd_bottom_sheet, null);

                TextView tvTopic = (TextView)view.findViewById(R.id.tvTopic);
                TextView tvDate = (TextView)view.findViewById(R.id.tvDate);
                TextView tvVenue = (TextView)view.findViewById(R.id.tvVenue);
                TextView tvTime = (TextView)view.findViewById(R.id.tvTime);
                Button btnDownload =(Button)view.findViewById(R.id.btnDownload);

                //ImageView imageView = (ImageView)view.findViewById(R.id.imageView);


                final Dialog mBottomSheetDialog = new Dialog (context);
                Window window = mBottomSheetDialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);

                // BottomSheet

                mBottomSheetDialog.setContentView (view);
                mBottomSheetDialog.setCancelable (true);
                mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow ().setGravity (Gravity.BOTTOM);
                mBottomSheetDialog.show ();
                tvTopic.setText(cpdMonitorings.getTopic()) ;
                tvDate.setText(cpdMonitorings.getDate()) ;
                tvVenue.setText(cpdMonitorings.getVenue()) ;
                tvTime.setText(cpdMonitorings.getTimeFrom() + " - " + cpdMonitorings.getTimeTo()) ;
                btnDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toasty.warning(context, "No available certificate yet");
                    }
                });


            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTopic, tvDate, tvVenue, tvTime;
        Button btnView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTopic = (TextView) itemView.findViewById(R.id.tvTopic);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            tvVenue = (TextView) itemView.findViewById(R.id.tvVenue);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);

        }
    }
}
