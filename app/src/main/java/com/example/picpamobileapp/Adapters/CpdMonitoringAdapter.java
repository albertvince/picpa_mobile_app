package com.example.picpamobileapp.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.picpamobileapp.Models.CpdMonitoring;
import com.example.picpamobileapp.Models.Speakers;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmResults;


public class CpdMonitoringAdapter extends RecyclerView.Adapter<CpdMonitoringAdapter.ViewHolder> {

    private Context mContext;
    private List<CpdMonitoring> mList;
    private ItemClickListener mClickListener;

    public CpdMonitoringAdapter(@NonNull Context context, RealmResults<CpdMonitoring> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_cpd_monitoring,viewGroup, false);

        CpdMonitoringAdapter.ViewHolder viewHolder = new CpdMonitoringAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        final CpdMonitoring cpdMonitoring = mList.get(i);

        TextView tvTopic, tvDate, tvVenue, tvTime;

        tvTopic = holder.tvTopic;
        tvDate = holder.tvDate;
        tvVenue = holder.tvVenue;
        tvTime = holder.tvTime;

        tvTopic.setText(cpdMonitoring.getTopic());
        Debugger.printO(cpdMonitoring.getTopic());
        tvDate.setText(cpdMonitoring.getDate());
        tvVenue.setText(cpdMonitoring.getVenue());
        tvTime.setText(cpdMonitoring.getTimeFrom() + " - " + cpdMonitoring.getTimeTo());

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener{
        TextView tvTopic, tvDate, tvVenue, tvTime;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTopic = (TextView) itemView.findViewById(R.id.tvTopic);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            tvVenue = (TextView) itemView.findViewById(R.id.tvVenue);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);

            cardView = itemView.findViewById(R.id.cv_cpd);

            cardView.setOnClickListener(this);
            cardView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }

    public CpdMonitoring getItem(int id) {
        return mList.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
