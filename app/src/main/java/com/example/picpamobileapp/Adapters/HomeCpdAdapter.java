package com.example.picpamobileapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.picpamobileapp.Interfaces.RecyclerViewClick;
import com.example.picpamobileapp.R;

import java.util.ArrayList;

public class HomeCpdAdapter  extends RecyclerView.Adapter<HomeCpdAdapter.MyViewHolder> {
    private Context context;
    private LayoutInflater layoutInflater;

    private ArrayList<String> images = new ArrayList<>();
    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<Float> ratings = new ArrayList<>();

    private RecyclerViewClick listener;

    public HomeCpdAdapter(Context context, ArrayList<String> images, ArrayList<String> names, ArrayList<Float> ratings, RecyclerViewClick listener) {
        this.context = context;
        this.images = images;
        this.names = names;
        this.ratings = ratings;
        this.listener = listener;
        layoutInflater = LayoutInflater.from(context);
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.listrow_home_cpd, viewGroup, false);
        final MyViewHolder myViewHolder = new MyViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, myViewHolder.getAdapterPosition());
            }
        });

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        RequestOptions myOption = new RequestOptions()
                .centerInside();
        Glide.with(context)
                .load(images.get(i))
                .apply(myOption)
                .into(myViewHolder.imageView);

        myViewHolder.tvName.setText(names.get(i));
        myViewHolder.tvRate.setText(String.valueOf(ratings.get(i)));
        myViewHolder.ratingBar.setRating(ratings.get(i));
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView tvName, tvRate;
        private RatingBar ratingBar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_MostWantedImage);
            tvName = itemView.findViewById(R.id.tv_MostWantedName);
            tvRate = itemView.findViewById(R.id.tv_MostWantedRate);
            ratingBar = itemView.findViewById(R.id.ratingBar_MostWanted);
        }
    }
}
