package com.example.picpamobileapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.picpamobileapp.Models.UpcomingImagesModel;
import com.example.picpamobileapp.R;

import java.util.ArrayList;

public class UpcomingSeminarViewPagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<UpcomingImagesModel> upcomingImagesModelArrayList = new ArrayList<>();

    public UpcomingSeminarViewPagerAdapter(Context context, ArrayList<UpcomingImagesModel> upcomingImagesModelArrayList) {
        this.context = context;
        this.upcomingImagesModelArrayList = upcomingImagesModelArrayList;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return upcomingImagesModelArrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = layoutInflater.inflate(R.layout.listrow_home_final, null);

        ImageView imageView = view.findViewById(R.id.iv_HomeSeminarImages);

        RequestOptions myOption = new RequestOptions()
                .centerInside();
        Glide.with(context)
                .load(getImageUrl() + upcomingImagesModelArrayList.get(position).getImage())
//                .load("https://registration.picpacdomisor.org/images/carousel/aww.png")
                .apply(myOption)
                .into(imageView);

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;
    }

    private String getImageUrl() {
        return "https://registration.picpacdomisor.org/images/carousel/";
    }
}

