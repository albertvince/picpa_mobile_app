package com.example.picpamobileapp.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.model.ByteArrayLoader;
import com.example.picpamobileapp.Models.UpcomingSeminar;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Converter;
import com.example.picpamobileapp.Utils.Debugger;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmResults;

public class UpcomingSeminarAdapter extends RecyclerView.Adapter<UpcomingSeminarAdapter.ViewHolder> {

    private Context mContext;
    private List<UpcomingSeminar> mList;
    private ItemClickListener mClickListener;


    public UpcomingSeminarAdapter(@NonNull Context context, RealmResults<UpcomingSeminar> list) {
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_upcoming_seminar,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UpcomingSeminarAdapter.ViewHolder holder, int i) {

        final UpcomingSeminar upcomingSeminar = mList.get(i);

        TextView tvTopic, tvDate, tvTime, text4;

        tvTopic = holder.tvTopic;
        tvDate = holder.tvDate;
        tvTime = holder.tvTime;

        tvTopic.setText( upcomingSeminar.getSeminartopic());
        tvDate.setText( upcomingSeminar.getNewdate());
        tvTime.setText( upcomingSeminar.getFromtime() + " - " + upcomingSeminar.getTotime() );

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener, View.OnCreateContextMenuListener{

        Button btnView;
        CardView cardView;
        TextView tvTopic, tvTime, tvDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTopic = (TextView) itemView.findViewById(R.id.tvTopic);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            cardView = itemView.findViewById(R.id.cv_seminar);

            cardView.setOnClickListener(this);
            cardView.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }

    public UpcomingSeminar getItem(int id) {
        return mList.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
