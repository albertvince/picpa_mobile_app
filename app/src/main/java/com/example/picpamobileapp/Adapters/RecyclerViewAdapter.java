package com.example.picpamobileapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.picpamobileapp.Models.OngoingImages;
import com.example.picpamobileapp.Models.UpcomingImages;
import com.example.picpamobileapp.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private LayoutInflater layoutInflater;
   // private ArrayList<OngoingImages> images = new ArrayList<>();
    private Context mContext;
    private List<OngoingImages> mList;
    private Fragment fragment;

    public RecyclerViewAdapter(Context context, List<OngoingImages> list, Fragment fragment){
        mContext = context;
        mList = list;
        this.fragment = fragment;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listrow_home_ex, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        CircularImageView image ;

        final OngoingImages ongoingImages = mList.get(i);

        RequestOptions myOption = new RequestOptions()
                .centerInside();
        Glide.with(fragment)
                .load("https://picpacdomisor.org/images/carousel/"+ongoingImages.getImage_name())
                .apply(myOption)
                .into(viewHolder.image);
       // viewHolder.name.setText(names.get(i));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CircularImageView image;
        TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
        }
    }
}
