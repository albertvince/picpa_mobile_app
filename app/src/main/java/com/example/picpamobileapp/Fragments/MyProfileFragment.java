package com.example.picpamobileapp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.picpamobileapp.Adapters.CpdMonitoringAdapter;
import com.example.picpamobileapp.Activities.UserProfileActivity;
import com.example.picpamobileapp.Models.CpdMonitoring;
import com.example.picpamobileapp.Models.UserProfile;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.Syncer;
import java.util.ArrayList;

import io.realm.Realm;

public class MyProfileFragment extends Fragment {

    //Context View
    private View view;
    private Context context;

    //Widgets
    private Button btnUpdate;
    private ListView lvRecords;
    private View emptyIndicator;
    private ImageView imgProfile;
    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;
    private TextView txtContact, txtPrcLicense, txtDateRegistered, txtAddress, txtFacebook, txtEmail, txtCompanyName, txtName, tvOpenGallery;

    //Data
    private Realm realm;
    private String imagePath = "";
    private ArrayList<CpdMonitoring> evalList;

    //Adapter
    private CpdMonitoringAdapter cpdMonitoringAdapter;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_myprofile, container, false);
        context = view.getContext();
        realm = Realm.getDefaultInstance();

        getActivity().setTitle("My Profile");

        UserProfile userProfile = realm.where(UserProfile.class).findFirst();

        if (userProfile == null){
            Syncer.syncUserProfile(getContext(), false, getActivity());
        }
        else {
            initializeUI();
        }

        return view;
    }

    @Override
    public void onStart() {
        setHasOptionsMenu(true);
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

        UserProfile userProfile = realm.where(UserProfile.class).findFirst();

        if (userProfile != null) {
            initializeUI();
        }
    }

    private void initializeUI(){
        //final UserProfile userProfiles = new UserProfile();

        btnUpdate = view.findViewById(R.id.btnUpdate);
        txtContact = view.findViewById(R.id.txtContact);
        txtPrcLicense = view.findViewById(R.id.txtPrcLicense);
        txtDateRegistered = view.findViewById(R.id.txtDateReg);
        txtAddress = view.findViewById(R.id.txtAddress);
        txtFacebook = view.findViewById(R.id.txtFacebook);
        txtEmail = view.findViewById(R.id.txtGmail);
        txtCompanyName = view.findViewById(R.id.txtCompany);
        txtName = view.findViewById(R.id.txtName);
        imgProfile= view.findViewById(R.id.img_profile);

        UserProfile userProfile = realm.where(UserProfile.class).findFirst();
        assert userProfile != null;
        userProfile.load();

        if(userProfile.isLoaded()) {
            txtName.setText(userProfile.getFirst_name() + " " + userProfile.getLast_name() + " " + userProfile.getSuffixes());
            txtContact.setText(userProfile.getContact_no());
            txtPrcLicense.setText(userProfile.getRegister_no());
            txtDateRegistered.setText(userProfile.getRegister_date());
            txtAddress.setText(userProfile.getBilling_address());
            txtFacebook.setText(userProfile.getFacebook_acc());
            txtEmail.setText(userProfile.getEmail_add());
            txtCompanyName.setText(userProfile.getCompany_name());

            Debugger.logD("getUser_image "+userProfile.getUser_image());

            if(userProfile.getUser_image() != null && userProfile.getUser_image().length() > 0) {
                if (userProfile.getUser_image().contains("storage")) {
                    displayImage(userProfile.getUser_image());
                    Debugger.logD("11");
                } else {
                    displayImage("https://registration.picpacdomisor.org/userimages/" + userProfile.getUser_image());
                    Debugger.logD("22");
                }
            }

        }
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), UserProfileActivity.class);
                startActivity(intent);
            }
        });

    }


    //Display list of image using Glide
    private void displayImage(String imagePath) {
        RequestOptions myOption = new RequestOptions()
                .dontTransform()
                .circleCrop();

        Glide.with(context)
                .load(imagePath)
                .apply(myOption)
                .into(imgProfile);

    }
}
