package com.example.picpamobileapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.picpamobileapp.Models.Chapter;
import com.example.picpamobileapp.Models.UserProfile;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.Syncer;
import com.example.picpamobileapp.Utils.Utility;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class ProfileCompanyFragment extends Fragment {

    //Context and View
    private View view;
    private Context context;

    //Data
    private Realm realm;
    private UserProfile userProfile;
    private Chapter selectedChapter;

    //Widgets
    private Button btnSave;
    private Spinner spinnerSector, spinnerChapter;
    private EditText etCompanyName, etBillingAddress;

    public ProfileCompanyFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile_company, container,false);
        context = getContext();
        realm = Realm.getDefaultInstance();


        initializeUI();
        readRecords();
        return view;
    }

    private void initializeUI(){

        spinnerChapter = view.findViewById(R.id.sp_chapter);
        etCompanyName = view.findViewById(R.id.etCompanyName);
        spinnerSector = view.findViewById(R.id.spinnerSector);
        btnSave = view.findViewById(R.id.btn_save_company_details);
        etBillingAddress = view.findViewById(R.id.etBillingAddress);

        RealmResults<Chapter> realmResults = realm.where(Chapter.class).findAll();
        List<Chapter> chapters = realm.copyFromRealm(realmResults);
        final ArrayAdapter<Chapter> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, chapters);
        spinnerChapter.setAdapter(adapter);

        spinnerChapter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedChapter = adapter.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        userProfile = realm.where(UserProfile.class).findFirst();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utility.haveNetworkConnection(context, getFragmentManager())) {
                    updateUserCompanyDetails();
                    new Syncer.UpdateProfileDetails(context, userProfile, false, false).execute();
                }
            }
        });

    }

    private void readRecords() {

        try{

            etCompanyName.setText(userProfile.getCompany_name());
            spinnerSector.setSelection(userProfile.getSector() - 1);
            etBillingAddress.setText(userProfile.getBilling_address());

            RealmResults<Chapter> realmResults = realm.where(Chapter.class).findAll();
            List<Chapter> chapters = realm.copyFromRealm(realmResults);

            int chapterId = 0;
            for(int i = 0; i < chapters.size(); i++){
                if(Integer.parseInt(userProfile.getChapter()) == chapters.get(i).getChapterId()){
                    chapterId = i;
                }
            }
            spinnerChapter.setSelection(chapterId);

        }catch (Exception err){
            Debugger.logD("Exception err "+err);
        }
    }

    private void updateUserCompanyDetails(){

        final int userId;
        final UserProfile userProfile1 = realm.where(UserProfile.class).findFirst();
        userId = userProfile1.getId();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                UserProfile userProfile = realm.where(UserProfile.class).equalTo("id", userId).findFirst();
                if (userProfile != null) {
                    userProfile.setCompany_name(etCompanyName.getText().toString());
                    userProfile.setSector(spinnerSector.getSelectedItemPosition() + 1);
                    userProfile.setBilling_address(etBillingAddress.getText().toString());
                    userProfile.setChapter(String.valueOf(selectedChapter.getChapterId()));
                }
            }
        });
    }

    public static ProfileCompanyFragment newInstance() {
        ProfileCompanyFragment fragment = new ProfileCompanyFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
}
