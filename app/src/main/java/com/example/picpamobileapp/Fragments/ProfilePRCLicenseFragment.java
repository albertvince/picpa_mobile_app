package com.example.picpamobileapp.Fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.picpamobileapp.Activities.CameraActivity;
import com.example.picpamobileapp.Models.UserProfile;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.DateTimeHandler;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.Syncer;
import com.example.picpamobileapp.Utils.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;

import static android.app.Activity.RESULT_OK;

public class ProfilePRCLicenseFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    //Context View
    private View view;
    private Context context;

    //Data
    private Realm realm;
    private Uri cameraFiles;
    private String cameraPath;
    private Calendar calendar;
    private String picturePath;
    private String takenPhotoPath;
    private final int CAMERA = 997;
    private UserProfile userProfile;
    private boolean isChecked = false;
    private boolean isValidDateSelected;
    private boolean isRegisteredDateSelected;
    private final int CODE_GALLERY_REQUEST = 999;
    private final int OPEN_CAMERA_ACTIVITY = 998;

    //Widgets
    private Button btnSave;
    private ImageView imgPrc;
    private TextView tvUpload;
    private EditText etPRCNo, etRegDate, etValidDate;


    public ProfilePRCLicenseFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile_prclicense, container,false);
        context = getContext();

        realm = Realm.getDefaultInstance();
        initializeUI();
        readRecords();

        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void initializeUI(){
        imgPrc = view.findViewById(R.id.img_prc);
        etPRCNo = view.findViewById(R.id.etPRCNo);
        btnSave = view.findViewById(R.id.btn_save_prc);
        etValidDate = view.findViewById(R.id.etValidUntil);
        tvUpload = view.findViewById(R.id.tv_select_photo);
        etRegDate = view.findViewById(R.id.etRegisteredDate);
        setDates();

        userProfile = realm.where(UserProfile.class).findFirst();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utility.haveNetworkConnection(context, getFragmentManager())) {
                    updateUserPrcDetails();
                    new Syncer.UpdateProfileDetails(context, userProfile, false, true).execute();
                }

            }
        });
        calendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener regdate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etRegDate.setText(DateTimeHandler.convertDatetoDisplayDate(calendar.getTime()));
            }
        };

        final DatePickerDialog.OnDateSetListener validdate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etValidDate.setText(DateTimeHandler.convertDatetoDisplayDate(calendar.getTime()));
            }
        };

        etRegDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                new DatePickerDialog(getActivity(), regdate, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
//                isRegisteredDateSelected = true;
//                DatePickers.DatePickerForFragment fragment = new DatePickers.DatePickerForFragment();
//                fragment.show(getChildFragmentManager(), "date");
            }
        });

        etValidDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                new DatePickerDialog(getActivity(), validdate, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
//
//        etRegDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus){
//                    isRegisteredDateSelected = true;
//                    DatePickers.DatePickerForFragment fragment = new DatePickers.DatePickerForFragment();
//                    fragment.show(getChildFragmentManager(), "date");
//                }
//            }
//        });

        tvUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPicture();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CODE_GALLERY_REQUEST && resultCode == RESULT_OK && data != null) {
            try {
                Uri selectedImage = data.getData();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = context.getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();

                displayImage(picturePath);

                Toasty.success(context, "Photo Selected", Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                Debugger.logD("onActivityResult Exception "+e);
            }
        }
    }

    private void updateUserPrcDetails(){

        final int userId;
        final UserProfile userProfile1 = realm.where(UserProfile.class).findFirst();
        userId = userProfile1.getId();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                UserProfile userProfile = realm.where(UserProfile.class).equalTo("id", userId).findFirst();
                if (userProfile != null) {
                    userProfile.setPrc_image(picturePath);
                    Debugger.logD("picturePath1 "+picturePath);
                    userProfile.setRegister_no(etPRCNo.getText().toString());
                    userProfile.setRegister_date(etRegDate.getText().toString());
                    userProfile.setValid_until(etValidDate.getText().toString());
                }
            }
        });
    }

    //Display list of image using Glide
    private void displayImage(String imagePath) {
        RequestOptions myOption = new RequestOptions()
                .dontTransform();

        Glide.with(context)
                .load(imagePath)
                .apply(myOption)
                .into(imgPrc);

    }

    public static String getName(String filePath) {
        if (filePath == null || filePath.length() == 0) {
            return "";
        }
        int extract = filePath.lastIndexOf('?');
        if (extract > 0) {
            filePath = filePath.substring(0, extract);
        }
        int namePos = filePath.lastIndexOf(File.separatorChar);
        return (namePos >= 0) ? filePath.substring(namePos + 1) : filePath;
    }

    public void addPicture() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, CODE_GALLERY_REQUEST);
    }

    private void takePhoto() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE_SECURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraFiles);
        startActivityForResult(cameraIntent, CAMERA);
    }

    private void setDates()
    {
        Calendar calendar = Calendar.getInstance();

        Date date = calendar.getTime();

        calendar.add(Calendar.DATE, 30);

//        member.setBirthDate(date);

        etRegDate.setText(DateTimeHandler.convertDatetoDisplayDate(date));

    }

    private void readRecords() {

        try{

            etPRCNo.setText(userProfile.getRegister_no());
            etRegDate.setText(userProfile.getRegister_date());
            etValidDate.setText(userProfile.getValid_until());
            userProfile.load();
            Debugger.logD("getPrc_image "+userProfile.getPrc_image());

            if (userProfile.getPrc_image() != null) {
                if(userProfile.getPrc_image().contains("storage")){
                    displayImage(userProfile.getPrc_image());
                } else{
                    displayImage("https://registration.picpacdomisor.org/userimages/"+userProfile.getPrc_image());
                }
            }

        }catch (Exception err){
            Debugger.logD("ProfilePRCLicenseFragment readRecords err: "+err);
        }

    }

    public static ProfilePRCLicenseFragment newInstance() {
        ProfilePRCLicenseFragment fragment = new ProfilePRCLicenseFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        try
        {
            Calendar cal = new GregorianCalendar(year, month, dayOfMonth);

            if(isRegisteredDateSelected)
            {
                etRegDate.setText(DateTimeHandler.convertDatetoDisplayDate(cal.getTime()));
                etRegDate.setError(null);
//                this.member.setBirthDate(cal.getTime());
            }

        } catch (Exception err){
            Toasty.error(context, err.toString()).show();
        }
    }

    public void hideKeyboard()
    {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
