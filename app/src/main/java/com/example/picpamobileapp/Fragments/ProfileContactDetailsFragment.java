package com.example.picpamobileapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.picpamobileapp.Models.UserProfile;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Syncer;
import com.example.picpamobileapp.Utils.Utility;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;

public class ProfileContactDetailsFragment extends Fragment {

    //Context View
    private View view;
    private Context context;

    //Data
    private Realm realm;
    private UserProfile userProfile;

    //Widgets
    private Button btnSave;
    private EditText etEmail, etFB, etContactNo, etAddress;

    public ProfileContactDetailsFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile_contactdetails, container,false);
        context = getContext();
        realm = Realm.getDefaultInstance();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeUI();
        readRecords("");
    }

    private void initializeUI(){

        etFB = view.findViewById(R.id.etFB);
        etEmail = view.findViewById(R.id.etEmail);
        etAddress = view.findViewById(R.id.etAddress);
        etContactNo = view.findViewById(R.id.etContactNo);
        btnSave = view.findViewById(R.id.btn_save_contact_details);

        userProfile = realm.where(UserProfile.class).findFirst();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utility.haveNetworkConnection(context, getFragmentManager())) {
                    updateUserContactDetails();
                    new Syncer.UpdateProfileDetails(context, userProfile, false, false).execute();
                }
            }
        });
    }

    private void readRecords(String search) {

        etEmail.setText(userProfile.getEmail_add());
        etFB.setText(userProfile.getFacebook_acc());
        etContactNo.setText(userProfile.getContact_no());
        etAddress.setText(userProfile.getFull_address());
    }

    private void updateUserContactDetails(){

        final int userId;
        final UserProfile userProfile1 = realm.where(UserProfile.class).findFirst();
        userId = userProfile1.getId();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                UserProfile userProfile = realm.where(UserProfile.class).equalTo("id", userId).findFirst();
                if (userProfile != null) {
                    userProfile.setEmail_add(etEmail.getText().toString());
                    userProfile.setFacebook_acc(etFB.getText().toString());
                    userProfile.setContact_no(etContactNo.getText().toString());
                    userProfile.setFull_address(etAddress.getText().toString());
                }
            }
        });
    }

    public static ProfileContactDetailsFragment newInstance() {
        ProfileContactDetailsFragment fragment = new ProfileContactDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
}
