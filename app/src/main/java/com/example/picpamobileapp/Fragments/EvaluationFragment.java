package com.example.picpamobileapp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.picpamobileapp.Activities.SpeakerListActivity;
import com.example.picpamobileapp.Adapters.EvaluationAdapter;
import com.example.picpamobileapp.Models.Evaluation;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.HttpProvider;
import com.example.picpamobileapp.Utils.UserSession;

import com.example.picpamobileapp.Utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

public class EvaluationFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, EvaluationAdapter.ItemClickListener {

    //Context View
    private View view;
    private Context context;

    //Adapter
    private RecyclerView recyclerView;
    private EvaluationAdapter evalAdapter;

    //Widgets
    private View emptyIndicator;
    private TextView tvNoRecords;
    private SwipeRefreshLayout swipeLayout;

    //Data
    private Realm realm;
    private String errorMessage = "";
    private Evaluation selectedEvaluation;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_evaluation, container, false);
        setHasOptionsMenu(true);
        context = view.getContext();
        realm = Realm.getDefaultInstance();

        initializeUI();

        getActivity().setTitle("Evaluation");
        syncData();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        syncData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search, menu);

        final MenuItem searchViewItem = menu.findItem(R.id.action_search);

        final SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                readRecords(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                readRecords(newText);
                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }


    public void initializeUI(){

        tvNoRecords = view.findViewById(R.id.tv_no_records);
        recyclerView = view.findViewById(R.id.recyclerView);
        swipeLayout = view.findViewById(R.id.swipe_container);
        emptyIndicator = view.findViewById(R.id.viewEmptyListIndicator);

        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorAccent), getResources().getColor(android.R.color.holo_red_dark), getResources().getColor(android.R.color.holo_blue_dark), getResources().getColor(android.R.color.holo_orange_dark));
        swipeLayout.setProgressViewOffset(false,
                getResources().getDimensionPixelSize(R.dimen.refresher_offset),
                getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));

        readRecords("");

    }

    private void syncData(){

        if(Utility.haveNetworkConnection(context)) {
            syncEvaluation(context);
        } else{
            swipeLayout.setRefreshing(false);
            Toasty.warning(context, context.getResources().getString(R.string.connect_to_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private void readRecords(String searchValue){

        RealmResults<Evaluation> evaluationRealmResults= realm.where(Evaluation.class).contains("seminar_topic", searchValue, Case.INSENSITIVE).findAll();
        evaluationRealmResults.load();

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        if(evaluationRealmResults.isLoaded()){
            evalAdapter = new EvaluationAdapter(context, evaluationRealmResults);
            evalAdapter.setClickListener(this);
            recyclerView.setAdapter(evalAdapter);

            showEmptyListIndicator(evaluationRealmResults.size() <= 0);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        selectedEvaluation = evalAdapter.getItem(position);

        Intent intent = new Intent();
        intent.setClass(context, SpeakerListActivity.class);
        intent.putExtra("SEMINAR_OBJECT", selectedEvaluation);
        startActivity(intent);

    }

    public void syncEvaluation(final Context context)
    {
        try{

            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));

            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/pendingevaluation", entity, true, new JsonHttpResponseHandler(){

                @Override
                public void onStart() {
                    super.onStart();
                    swipeLayout.setRefreshing(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        if (response != null){

                            Evaluation evaluation1 = new Evaluation();
                            evaluation1.delete();

                            ArrayList<Evaluation> evaluationArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<Evaluation>>(){}.getType());

                            for (Evaluation evaluation : evaluationArrayList)
                            {
                                evaluation.save(evaluation);
                            }

                            readRecords("");
                        }
                    }
                    catch (Exception err)
                    {
                        errorMessage = err.toString();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    errorMessage = context.getResources().getString(R.string.server_error);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    errorMessage = context.getResources().getString(R.string.server_error);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    errorMessage = context.getResources().getString(R.string.server_error);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    swipeLayout.setRefreshing(false);

                    if(!errorMessage.equals("")){
                        Toasty.warning(context , errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        catch (Exception err)
        {
            Toasty.warning(context , err.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    private void showEmptyListIndicator(boolean show)
    {
        emptyIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        tvNoRecords.setText(context.getResources().getString(R.string.all_done));

    }


    @Override
    public void onRefresh() {
        syncData();
    }

}
