package com.example.picpamobileapp.Fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.picpamobileapp.Adapters.UpcomingSeminarAdapter;
import com.example.picpamobileapp.Models.UpcomingSeminar;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.HttpProvider;
import com.example.picpamobileapp.Utils.UserSession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmResults;

public class UpseminarsFragment extends Fragment  implements SwipeRefreshLayout.OnRefreshListener, UpcomingSeminarAdapter.ItemClickListener{

    //View Context
    private View view;
    private Context context;
    private View emptyIndicator;

    //Adapter
    private UpcomingSeminarAdapter upcomingSeminarAdapter;

    //Data
    private Realm realm;
    private UpcomingSeminar selectedSeminar;

    //Widgets
    private TextView tvUpcomingSeminars, tvCountSeminar;
    private SwipeRefreshLayout swipeLayout;
    private RecyclerView recyclerView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_upseminars, container, false);
        context = getContext();

        return view;
    }

    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);

        realm = Realm.getDefaultInstance();

        initializeUI();
        new syncUpcomingSeminar(context).execute();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        realm.close();
    }

    public void initializeUI(){

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        emptyIndicator = (View) view.findViewById(R.id.viewEmptyListIndicator);
        tvUpcomingSeminars = view.findViewById(R.id.tvUpcomingSeminars);

        swipeLayout = view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorAccent), getResources().getColor(android.R.color.holo_red_dark), getResources().getColor(android.R.color.holo_blue_dark), getResources().getColor(android.R.color.holo_orange_dark));
        swipeLayout.setProgressViewOffset(false,
                getResources().getDimensionPixelSize(R.dimen.refresher_offset),
                getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));

        readRecords();

    }

    private void readRecords(){

        RealmResults<UpcomingSeminar> userRealmResults = realm.where(UpcomingSeminar.class).equalTo("isPaid", false).findAll();
        userRealmResults.load();

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        if (userRealmResults.isLoaded()) {

            upcomingSeminarAdapter = new UpcomingSeminarAdapter(context, userRealmResults);
            recyclerView.setAdapter(upcomingSeminarAdapter);
            upcomingSeminarAdapter.setClickListener(this);
            upcomingSeminarAdapter.notifyDataSetChanged();

            showEmptyListIndicator(userRealmResults.size() == 0);

            tvUpcomingSeminars.setText(String.valueOf(userRealmResults.size()));
        }

    }

    private void showEmptyListIndicator(boolean show)
    {
        emptyIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onRefresh() {

        new syncUpcomingSeminar(context).execute();
    }

    @Override
    public void onItemClick(View view, int position) {

        selectedSeminar = upcomingSeminarAdapter.getItem(position);

    }

    public class syncUpcomingSeminar extends AsyncTask<String, Integer, String> {

        private Context context;
        private String success = "", errorMessage = "";

        public syncUpcomingSeminar(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeLayout.setRefreshing(true);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            cancel(true);

            if (!success.equals("")) {
                Toasty.warning(context, "Failed Sync Seminar, " + errorMessage, Toast.LENGTH_LONG).show();
            }

            swipeLayout.setRefreshing(false);
        }

        @Override
        protected String doInBackground(final String... strings) {

            try {

                JSONObject jsonParams = new JSONObject();
                jsonParams.put("Token", UserSession.getToken(context));
                StringEntity entity = new StringEntity(jsonParams.toString());

                HttpProvider.postSync(context, "post/upcomingseminars", entity, true, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {

                            UpcomingSeminar.delete();

                            ArrayList<UpcomingSeminar> upcomingSeminarArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<UpcomingSeminar>>() {}.getType());

                            for (UpcomingSeminar upcomingSeminar : upcomingSeminarArrayList) {
                                upcomingSeminar.save(upcomingSeminar);
                            }

                        } catch (Exception err) {

                            errorMessage = context.getResources().getString(R.string.server_error);
                            Debugger.logD("Exception onSuccess "+err.toString());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        cancel(true);
                        errorMessage = responseString;
                        success = "FAILED";
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        errorMessage = context.getResources().getString(R.string.server_error);
                        cancel(true);
                        success = "FAILED";
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        cancel(true);
                        errorMessage = context.getResources().getString(R.string.server_error);
                        success = "FAILED";
                    }
                });

            } catch (Exception err) {
                cancel(true);
                errorMessage = "doInBackground Exception " + err.toString();
                return success = "FAILED";
            }

            return success;

        }

        @Override
        protected void onPostExecute(String success) {

            if (!success.isEmpty()) {
                Toasty.warning(context, "Failed Sync Seminar!", Toast.LENGTH_LONG).show();
            }

            readRecords();
            swipeLayout.setRefreshing(false);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

        }
    }


}
