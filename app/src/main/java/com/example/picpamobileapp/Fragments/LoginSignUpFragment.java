package com.example.picpamobileapp.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.picpamobileapp.MainActivity;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.HttpProvider;
import com.example.picpamobileapp.Utils.Syncer;
import com.example.picpamobileapp.Utils.UserSession;
import com.example.picpamobileapp.Utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class LoginSignUpFragment extends Fragment {
    private View view;
    private Context context;
    private EditText etEmail, etPassword, etConfirmPassword;
    private EditText etPRCNo;
    private CheckBox cbCPA;
    private ProgressDialog progressDialog;
    public LoginSignUpFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login_signup, container,false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
//        initializeUI();
        Button btnSignIn;
        progressDialog = new ProgressDialog(getActivity());
        etEmail = view.findViewById(R.id.etEmail);
        etPassword = view.findViewById(R.id.etPassword);
        etConfirmPassword = view.findViewById(R.id.etConfirmPassword);

        btnSignIn = view.findViewById(R.id.btnSignIn);
        etPRCNo = view.findViewById(R.id.etPRCNo);
        etPRCNo.setEnabled(false);

        cbCPA = view.findViewById(R.id.cbCPA);
        cbCPA.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    etPRCNo.setEnabled(true);
                    etPRCNo.setText("");
                }
                else {
                    etPRCNo.setEnabled(false);
                    etPRCNo.setText("");
                }
            }
        });
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
    }

    public static LoginSignUpFragment newInstance() {
        LoginSignUpFragment fragment = new LoginSignUpFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    private void register()
    {
        try
        {
            JSONObject jsonObject = new JSONObject();
            final ProgressDialog progressDialog = new ProgressDialog(context);
            jsonObject.put("email", etEmail.getText().toString());
            jsonObject.put("password", etPassword.getText().toString());
            jsonObject.put("prcno", etPRCNo.getText().toString());

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpProvider.post(context, "post/registeruser", stringEntity, "application/json", new JsonHttpResponseHandler(){

                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog.show();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    progressDialog.dismiss();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Debugger.logD(response.toString());
                    Toasty.success(context, "Successfully logged in!").show();
                    //Toast.makeText(getContext(),"Success",Toast.LENGTH_SHORT).show();
                    UserSession session = new Gson().fromJson(response.toString(), new TypeToken<UserSession>(){}.getType());
                    session.saveUserSession(getContext());
                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt("FIRST_LOGIN", 1);
                    editor.commit();

                    attemptLogin();

//                    Intent main_intent = new Intent(getContext(), MainActivity.class);
//                    main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(main_intent);
//                    getActivity().finish();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    Debugger.logD(response.toString());
                    //Toasty.success(context, "Success").show();
                   // Toast.makeText(getContext(),"Success",Toast.LENGTH_SHORT).show();
                    Toasty.success(context, "Successfully logged in!").show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    //Toasty.error(context, "Error From Server 1").show();
                    //Toast.makeText(getContext(),"Error From Server 1",Toast.LENGTH_SHORT).show();
                    Toasty.warning(context, "An error occurred.").show();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    //Toasty.error(context, "Error from Server 2").show();
                   // Toast.makeText(getContext(),"Error From Server 2",Toast.LENGTH_SHORT).show();
                    Toasty.warning(context, "An error occurred.").show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    //Toasty.error(context, responseString).show();
                    //Toast.makeText(getContext(),"Error From Server 3 " + responseString,Toast.LENGTH_SHORT).show();
                    Toasty.warning(context, "An error occurred.").show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode, headers, responseString);
                }
            });

        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
            //Toast.makeText(getContext(),"Error " + err.toString(),Toast.LENGTH_SHORT).show();
        }
    }

    private void attemptLogin(){
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("username", etEmail.getText().toString());
            jsonParams.put("password", etPassword.getText().toString());

            StringEntity entity = new StringEntity(jsonParams.toString());

            if(!Utility.haveNetworkConnection(context)){
                etEmail.setError("Internet connection is required");
                etEmail.requestFocus();
            }else {
                Debugger.printO(jsonParams.toString());
                doLogin(entity);
                hideKeyBoard();
            }
        } catch (Exception err) {
            Toasty.error(getContext(), err.toString()).show();
        }

    }


    private void hideKeyBoard(){
        try
        {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert inputManager != null;
            //inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doLogin(StringEntity stringEntity){
        progressDialog.show();
        progressDialog.setMessage("Logging In");
        HttpProvider.post(getContext(), "post/userlogin", stringEntity,"application/json", new JsonHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    boolean isSuccess = response.getBoolean("success");
                    if(isSuccess) {
                        progressDialog.hide();
                        Toasty.success(context, "You are now logged in ", Toast.LENGTH_SHORT).show();
                        UserSession session = new Gson().fromJson(response.toString(), new TypeToken<UserSession>() {
                        }.getType());


                        session.saveUserSession(context);
                        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putInt("FIRST_LOGIN", 1);
                        editor.commit();

                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    } else{
                        progressDialog.hide();
                        etPassword.setError(getString(R.string.error_field_credentials));
                        etPassword.requestFocus();
                    }

                }catch (Exception err){
                    Debugger.logD("login Error: "+err);
                    Toasty.warning(context, "An error occurred.").show();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progressDialog.hide();
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toasty.warning(context, "An error occurred.").show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                progressDialog.hide();
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toasty.warning(context, "An error occurred.").show();
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progressDialog.hide();
                super.onFailure(statusCode, headers, responseString, throwable);
                Toasty.warning(context, "An error occurred.").show();
            }
        });
    }

}
