package com.example.picpamobileapp.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.picpamobileapp.Adapters.HomeCpdAdapter;
import com.example.picpamobileapp.Adapters.HomeViewPagerAdapter;
import com.example.picpamobileapp.Adapters.OngoingSeminarViewPagerAdapter;
import com.example.picpamobileapp.Adapters.UpcomingSeminarViewPagerAdapter;
import com.example.picpamobileapp.Models.CpdMonitoring;
import com.example.picpamobileapp.Models.CpdMonitorings;
import com.example.picpamobileapp.Models.OngoingImages;
import com.example.picpamobileapp.Models.OngoingImagesModel;
import com.example.picpamobileapp.Models.UpcomingImages;
import com.example.picpamobileapp.Models.UpcomingImagesModel;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.HttpProvider;
import com.example.picpamobileapp.Utils.Syncer;
import com.example.picpamobileapp.Utils.UserSession;
import com.example.picpamobileapp.Utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import io.realm.Realm;
import io.realm.RealmResults;

public class HomeFragment extends Fragment {
    private Context context;
    private View view;
    private ImageView imageView2;
    private LinearLayout linearLayout;
    private BottomSheetBehavior bottomSheetBehavior;

    private FloatingActionButton floatingActionButton;
    private ViewPager viewPager;
    private RecyclerView recyclerView;
    private HomeCpdAdapter homeCpdAdapter;

    private RealmResults<UpcomingImages> upcomingImagesRealmResults;
    private RealmResults<OngoingImages> ongoingImagesRealmResults;

    private HomeViewPagerAdapter homeViewPagerAdapter;

    private CpdMonitoring cpdMonitoring;
    private CpdMonitorings cpdMonitorings;

    private TextView tvEmpty;


    private BottomSheetBehavior mBottomSheetBehavoir;
    private ArrayList<String> images = new ArrayList<>();
    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<Float> ratings = new ArrayList<>();
    private Realm realm;

    private LinearLayout textsplash, texthome, menus;
    private Animation frombottom;


    //NEW (FROM VANNE)
    private ProgressBar progressBarOngoing, progressBarUpcoming;
    private ViewPager viewPagerOngoing, viewPagerUpcoming;
    private OngoingImagesModel ongoingImagesModel;
    private UpcomingImagesModel upcomingImagesModel;
    private ArrayList<OngoingImagesModel> ongoingImagesModelArrayList = new ArrayList<>();
    private ArrayList<UpcomingImagesModel> upcomingImagesModelArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_final, container, false);
        realm = Realm.getDefaultInstance();

        getActivity().setTitle("Home");
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        context = getContext();
        initializeUI();

        //NEW (FROM VANNE)
        loadOngoingSeminars();
        loadUpcomingSeminars();
    }

    private void initializeUI() {
        Syncer.syncUpcomingImages(context, false);
        Syncer.syncOngoingImages(context, false);


        frombottom = AnimationUtils.loadAnimation(getContext(), R.anim.frombottom);

        progressBarOngoing = view.findViewById(R.id.progressBar_HomeOngoingSem);
        progressBarUpcoming = view.findViewById(R.id.progressBar_HomeUpcomingSem);
        viewPagerOngoing = view.findViewById(R.id.viewPager_HomeOngoingSem);
        viewPagerUpcoming = view.findViewById(R.id.viewPager_HomeUpcomingSem);

        tvEmpty = view.findViewById(R.id.tvEmpty);
    }

    //NEW (FROM VANNE)
    private void loadOngoingSeminars() {
        viewPagerOngoing.setVisibility(View.GONE);
        progressBarOngoing.setVisibility(View.VISIBLE);
        try {
            if (!Utility.haveNetworkConnection(context)) {
                throw new Exception("Internet connection is required");
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/ongoingimages", entity, true, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        ongoingImagesModelArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<ArrayList<OngoingImagesModel>>() {
                        }.getType());
                        displayOngoingSeminars();
                        viewPagerOngoing.setVisibility(View.VISIBLE);
                        progressBarOngoing.setVisibility(View.GONE);
                    } catch (Exception err) {
                        Debugger.logD(err.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    viewPagerOngoing.setVisibility(View.GONE);
                    progressBarOngoing.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    viewPagerOngoing.setVisibility(View.GONE);
                    progressBarOngoing.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    viewPagerOngoing.setVisibility(View.GONE);
                    progressBarOngoing.setVisibility(View.GONE);
                }
            });
        } catch (Exception err) {
            viewPagerOngoing.setVisibility(View.GONE);
            progressBarOngoing.setVisibility(View.GONE);
        }
    }

    //NEW (FROM VANNE)
    private void displayOngoingSeminars() {

        viewPagerOngoing.setAdapter(new OngoingSeminarViewPagerAdapter(context, ongoingImagesModelArrayList));

        setViewPagerIndicatorforOngoing();

        showEmptyListIndicator(ongoingImagesModelArrayList.size() <= 0);

    }

    private void showEmptyListIndicator(boolean show)
    {
        tvEmpty.setVisibility(show ? View.VISIBLE : View.GONE);
        viewPagerOngoing.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void setViewPagerIndicatorforOngoing() {
        final LinearLayout linearLayout = view.findViewById(R.id.linearLayout_HomeOngoingSem);
        addBottomDotsforOngoing(linearLayout, 0);

        viewPagerOngoing.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                addBottomDotsforOngoing(linearLayout, i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    //Add dot indicator to your viewpager
    private void addBottomDotsforOngoing(LinearLayout linearLayout, int currentPage) {
        TextView[] dots;
        dots = new TextView[ongoingImagesModelArrayList.size()];

        linearLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(context);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#AAAAAA"));
            linearLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#212121"));
    }

    private void loadUpcomingSeminars() {

        viewPagerUpcoming.setVisibility(View.GONE);
        progressBarUpcoming.setVisibility(View.VISIBLE);

        try {

            if (!Utility.haveNetworkConnection(context)) {
                throw new Exception("Internet connection is required");
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/upcomingimages", entity, true, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        upcomingImagesModelArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<UpcomingImagesModel>>() {
                        }.getType());
                        displayUpcomingSeminars();
                        viewPagerUpcoming.setVisibility(View.VISIBLE);
                        progressBarUpcoming.setVisibility(View.GONE);

                    } catch (Exception err) {
                        Debugger.logD(err.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    viewPagerUpcoming.setVisibility(View.GONE);
                    progressBarUpcoming.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    viewPagerUpcoming.setVisibility(View.GONE);
                    progressBarUpcoming.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    viewPagerUpcoming.setVisibility(View.GONE);
                    progressBarUpcoming.setVisibility(View.GONE);
                }
            });
        } catch (Exception err) {
            viewPagerUpcoming.setVisibility(View.GONE);
            progressBarUpcoming.setVisibility(View.GONE);
        }
    }

    //NEW (FROM VANNE)
    private void displayUpcomingSeminars() {
        viewPagerUpcoming.setAdapter(new UpcomingSeminarViewPagerAdapter(context, upcomingImagesModelArrayList));
        setViewPagerIndicatorforUpcoming();
    }

    private void setViewPagerIndicatorforUpcoming() {
        final LinearLayout linearLayout = view.findViewById(R.id.linearLayout_HomeUpcomingSem);
        addBottomDotsforUpcoming(linearLayout, 0);

        viewPagerUpcoming.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                addBottomDotsforUpcoming(linearLayout, i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    //Add dot indicator to your viewpager
    private void addBottomDotsforUpcoming(LinearLayout linearLayout, int currentPage) {
        TextView[] dots;
        dots = new TextView[upcomingImagesModelArrayList.size()];

        linearLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(context);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#AAAAAA"));
            linearLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#212121"));
    }


}
