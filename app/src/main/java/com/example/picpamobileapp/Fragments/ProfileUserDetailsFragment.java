package com.example.picpamobileapp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.picpamobileapp.Models.UserProfile;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.Syncer;
import com.example.picpamobileapp.Utils.Utility;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;

import static android.app.Activity.RESULT_OK;

public class ProfileUserDetailsFragment extends Fragment {

    //Context and View
    private View view;
    private Context context;

    //Widgets
    private Button btnSave;
    private ImageView imgProfile;
    private EditText etFirstName, etLastName, etMiddleName, etSuffix, etUsername, etPassword;

    //Data
    private Realm realm;
    private String picturePath;
    private UserProfile userProfile;
    private final int CODE_GALLERY_REQUEST = 999;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile_userdetails, container,false);
        context = getContext();
        realm = Realm.getDefaultInstance();

        initializeUI();
        readRecords();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void initializeUI(){
        userProfile = realm.where(UserProfile.class).findFirst();

        etSuffix = view.findViewById(R.id.etSuffix);
        etLastName = view.findViewById(R.id.etLname);
        etFirstName = view.findViewById(R.id.etFname);
        etMiddleName = view.findViewById(R.id.etMname);
        etPassword = view.findViewById(R.id.etPassword);
        etUsername = view.findViewById(R.id.etUsername);
        imgProfile = view.findViewById(R.id.img_profile);
        btnSave = view.findViewById(R.id.btn_save_details);

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPicture();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utility.haveNetworkConnection(context, getFragmentManager())) {
                    updateUserDetails();
                    new Syncer.UpdateProfileDetails(context, userProfile, true, false).execute();
                }
            }
        });

        hideKeyboard();
        new Syncer.UpdateChapter(context).execute();

    }

    private void readRecords() {
        etSuffix.setText(userProfile.getSuffixes());
        etUsername.setText(userProfile.getUsername());
        etPassword.setText(userProfile.getPassword());
        etLastName.setText(userProfile.getLast_name());
        etFirstName.setText(userProfile.getFirst_name());
        etMiddleName.setText(userProfile.getMiddle_name());

        if(userProfile.getUser_image() != null && userProfile.getUser_image().length() > 0) {
            if (userProfile.getUser_image().contains("storage")) {
                displayImage(userProfile.getUser_image());
            } else {
                displayImage("https://registration.picpacdomisor.org/userimages/" + userProfile.getUser_image());
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CODE_GALLERY_REQUEST && resultCode == RESULT_OK && data != null) {
            try {
                Uri selectedImage = data.getData();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = context.getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();
                displayImage(picturePath);

                Toasty.success(context, "Photo Selected", Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                Debugger.logD("onActivityResult Exception "+e);
            }
        }

    }

    public void addPicture() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, CODE_GALLERY_REQUEST);
    }

    public void updateUserDetails(){

        try{

           final int userId;
           final UserProfile userProfile1 = realm.where(UserProfile.class).findFirst();
           userId = userProfile1.getId();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    UserProfile userProfile = realm.where(UserProfile.class).equalTo("id", userId).findFirst();

                    if (userProfile != null) {
                        Debugger.logD("picturePath "+picturePath);
                        userProfile.setUser_image(picturePath);
                        userProfile.setSuffixes(etSuffix.getText().toString());
                        userProfile.setUsername(etUsername.getText().toString());
                        userProfile.setPassword(etPassword.getText().toString());
                        userProfile.setLast_name(etLastName.getText().toString());
                        userProfile.setFirst_name(etFirstName.getText().toString());
                        userProfile.setMiddle_name(etMiddleName.getText().toString());

                    }
                }
            });

        }catch (Exception e){
            Debugger.logD("updateUserDetails err "+e);
        }

    }

    //Display list of image using Glide
    private void displayImage(String imagePath) {
        RequestOptions myOption = new RequestOptions()
                .dontTransform()
                .circleCrop();

        Glide.with(context)
                .load(imagePath)
                .apply(myOption)
                .into(imgProfile);

    }

    public void hideKeyboard() {
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static ProfileUserDetailsFragment newInstance() {
        ProfileUserDetailsFragment fragment = new ProfileUserDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
}
