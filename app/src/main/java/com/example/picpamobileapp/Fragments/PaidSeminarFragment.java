package com.example.picpamobileapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.picpamobileapp.Adapters.UpcomingSeminarAdapter;
import com.example.picpamobileapp.Models.UpcomingSeminar;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;

import io.realm.Realm;
import io.realm.RealmResults;

public class PaidSeminarFragment extends Fragment implements UpcomingSeminarAdapter.ItemClickListener {

    //View Context
    private View view;
    private Context context;
    private View emptyIndicator;

    //Adapter
    private UpcomingSeminarAdapter upcomingSeminarAdapter;

    //Data
    private Realm realm;
    private UpcomingSeminar selectedSeminar;

    //Widgets
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeLayout;
    private TextView tvUpcomingSeminars, tvCountSeminarLabel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_upseminars, container, false);

        realm = Realm.getDefaultInstance();
        context = view.getContext();


        return view;
    }

    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);

        initializeUI();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        realm.close();
    }

    public void initializeUI(){

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        emptyIndicator = (View) view.findViewById(R.id.viewEmptyListIndicator);
        tvUpcomingSeminars = view.findViewById(R.id.tvUpcomingSeminars);
        tvCountSeminarLabel = view.findViewById(R.id.tv_count_seminar_label);
        swipeLayout = view.findViewById(R.id.swipe_container);

        swipeLayout.setEnabled(false);

        readRecords();

    }

    private void readRecords(){

        RealmResults<UpcomingSeminar> userRealmResults = realm.where(UpcomingSeminar.class).equalTo("isPaid", true).findAll();
        userRealmResults.load();

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        if (userRealmResults.isLoaded()) {

            upcomingSeminarAdapter = new UpcomingSeminarAdapter(context, userRealmResults);
            upcomingSeminarAdapter.setClickListener(this);
            recyclerView.setAdapter(upcomingSeminarAdapter);
            upcomingSeminarAdapter.notifyDataSetChanged();

            showEmptyListIndicator(userRealmResults.size() == 0);
            tvUpcomingSeminars.setText(String.valueOf(userRealmResults.size()));
        }

        tvCountSeminarLabel.setText(context.getString(R.string.paid_seminars));

    }

    private void showEmptyListIndicator(boolean show)
    {
        emptyIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onItemClick(View view, int position) {

        selectedSeminar = upcomingSeminarAdapter.getItem(position);

    }
}
