package com.example.picpamobileapp.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.picpamobileapp.Activities.CpdMonitoringActivity;
import com.example.picpamobileapp.Adapters.CpdMonitoringAdapter;
import com.example.picpamobileapp.Adapters.CreatedCpdMonitoringAdapter;
import com.example.picpamobileapp.DialogFragments.CpdMonitoringDialog;
import com.example.picpamobileapp.DialogFragments.OpenSettingsDialog;
import com.example.picpamobileapp.Models.Competencies;
import com.example.picpamobileapp.Models.CpdMonitoring;
import com.example.picpamobileapp.Models.CpdMonitorings;
import com.example.picpamobileapp.Models.SubTopics;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.HttpProvider;
import com.example.picpamobileapp.Utils.UserSession;
import com.example.picpamobileapp.Utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmResults;

public class CpdMonitoringFragment extends Fragment implements CpdMonitoringAdapter.ItemClickListener {
    private View view;
    private Context context;

    private FloatingActionButton floatingActionButton;
    private RecyclerView recyclerView, recyclerView1;
    private ArrayList<CpdMonitoring> evalList;
    private CpdMonitoringAdapter cpdMonitoringAdapter;
    private CreatedCpdMonitoringAdapter createdCpdMonitoringAdapter;
    private View emptyIndicator, emptyIndicator1;

    private ListView lvRecords;
    private SearchView searchView;
    private LinearLayout view_bars;
    private CardView cardView1, cardView2, cardView3, cardView4;

    private TextView tvA, tvB, tvC, tvTotal, tvTotalCpdUnits;

    private CpdMonitoring selectedCpd;
    private CpdMonitorings cpdMonitorings;
    private SubTopics subTopics;

    private NestedScrollView lvCpd, lvCpd1;

    private boolean searchBarClicked = false;

    private Realm realm;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cpdmonitoring, container, false);
        realm = Realm.getDefaultInstance();

        getActivity().setTitle("Records");
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        context = getContext();
        syncAll();
    }

    private void syncAll(){
        initializeUI();
        syncCpdMonitoring(context);
        syncCpdMonitoringCompetencies(context);
        syncCpdMonitorings(context);
    }

    private void initializeUI(){
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView1 = (RecyclerView)view.findViewById(R.id.recyclerView1);
        emptyIndicator = (View) view.findViewById(R.id.viewEmptyListIndicator);
        emptyIndicator1 = (View) view.findViewById(R.id.viewEmptyListIndicator1);
        view_bars = (LinearLayout)view.findViewById(R.id.view_bars);
        searchView = view.findViewById(R.id.etSearch);

        lvCpd = view.findViewById(R.id.lvCpd);
        lvCpd1 = view.findViewById(R.id.lvCpd1);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                readRecords(query);
//                readRecords1(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                readRecords(newText);
//                readRecords1(newText);
                return false;
            }
        });

        tvA = view.findViewById(R.id.tvCompetencyA);
        tvB = view.findViewById(R.id.tvCompetencyB);
        tvC = view.findViewById(R.id.tvCompetencyC);
        tvTotal = view.findViewById(R.id.tvCompetencyTotal);
        tvTotalCpdUnits = view.findViewById(R.id.tvTotalCpdUnits);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {

            case R.id.action_search:
                showSearchBar();
                break;
            case R.id.update:
                //updateStatus();
//                readRecords1("");
                syncAll();
                break;
            case R.id.action_add:
                addSeminar();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //Cpd Seminar
    public void readRecords(String search) {
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<CpdMonitoring> cpdMonitoringRealmResults = realm.where(CpdMonitoring.class).findAll();
            cpdMonitoringRealmResults.load();

            if (cpdMonitoringRealmResults.isLoaded()) {
                LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                RecyclerView.LayoutManager rvLayoutManager = layoutManager;
                recyclerView.setLayoutManager(rvLayoutManager);

                cpdMonitoringAdapter = new CpdMonitoringAdapter(getContext(), cpdMonitoringRealmResults);
                cpdMonitoringAdapter.setClickListener(this);
                recyclerView.setAdapter(cpdMonitoringAdapter);

                cpdMonitoringAdapter.notifyDataSetChanged();

                showEmptyListIndicator(cpdMonitoringRealmResults.size() <= 0);


            }
        }
        catch(Exception err)
        {
            Debugger.logD(err.toString());
        }
    }
    private void showEmptyListIndicator(boolean show)
    {
        emptyIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        lvCpd.setVisibility(show ? View.GONE : View.VISIBLE);
        Debugger.printO("lvcpd is showing");

    }

    //NEW CREATED CPD MONITORING SEMINAR
    public void readRecords1(String search)
    {
        try{
            realm = Realm.getDefaultInstance();
            RealmResults<CpdMonitorings> cpdMonitoringsRealmResults = realm.where(CpdMonitorings.class).findAll();
            cpdMonitoringsRealmResults.load();

            if (cpdMonitoringsRealmResults.isLoaded()) {

                LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                RecyclerView.LayoutManager rvLayoutManager = layoutManager;
                recyclerView1.setLayoutManager(rvLayoutManager);

                createdCpdMonitoringAdapter = new CreatedCpdMonitoringAdapter(getContext(), cpdMonitoringsRealmResults);
                recyclerView1.setAdapter(createdCpdMonitoringAdapter);
                createdCpdMonitoringAdapter.notifyDataSetChanged();

                showEmptyListIndicator1(cpdMonitoringsRealmResults.size() <= 0);
            }

        }
        catch(Exception err)
        {
            Debugger.logD(err.toString());
        }

        loadSubTopics();
    }

    @Override
    public void onItemClick(View view, int position) {
        selectedCpd = cpdMonitoringAdapter.getItem(position);

        Bundle bundle = new Bundle();
        bundle.putParcelable("MONITORING_OBJECT", selectedCpd);

        CpdMonitoringDialog cpdMonitoringDialog = new CpdMonitoringDialog();
        cpdMonitoringDialog.setArguments(bundle);
        cpdMonitoringDialog.show(getFragmentManager(),"CPD");
    }

    public void loadSubTopics(){
        try {

            realm = Realm.getDefaultInstance();
            RealmResults<Competencies> competenciesRealmResults = realm.where(Competencies.class).findAll();
            competenciesRealmResults.load();

//            RealmResults<CpdMonitorings> cpdMonitoringsRealmResults = realm.where(CpdMonitorings.class).findAll();
//            cpdMonitoringsRealmResults.load();
//
//            int total = 0;
//            int customtotal = 0;
//            int subtopictotal = 0;
//            int totalA = 0;
//            int totalB = 0;
//            int totalC = 0;
//
//            for (int i = 0; i < cpdMonitoringsRealmResults.size(); i++){
//                total += Integer.parseInt(cpdMonitoringsRealmResults.get(i).getSeminarUnits());
//                customtotal += Integer.parseInt(cpdMonitoringsRealmResults.get(i).getSeminarUnits());
//
//                if (cpdMonitoringsRealmResults.get(i).getCompetency() == 1){
//                    totalA = totalA +1;
//                }
//                else if (cpdMonitoringsRealmResults.get(i).getCompetency() == 2){
//                    totalB = totalB +1;
//                }
//                else {
//                    totalC = totalC + 1;
//                }
//            }

//            for (int i = 0; i < competenciesRealmResults.size(); i++){
//                total += competenciesRealmResults.get(i).getTotalCPD();
//                subtopictotal += competenciesRealmResults.get(i).getTotalCPD();
//
////                if (competenciesRealmResults.get(i).get() == 1){
////                    totalA = totalA +1;
////                }
////                else if (subTopicsRealmResults.get(i).getSeminar_competency() == 2){
////                    totalB = totalB +1;
////                }
////                else {
////                    totalC = totalC + 1;
////                }
//            }


            tvTotal.setText(String.valueOf(competenciesRealmResults.get(0).getTotalCPD()));
            tvTotalCpdUnits.setText(String.valueOf(competenciesRealmResults.get(0).getTotalCPD()));
            tvA.setText(String.valueOf(competenciesRealmResults.get(0).getTotalA()));
            tvB.setText(String.valueOf(competenciesRealmResults.get(0).getTotalB()));
            tvC.setText(String.valueOf(competenciesRealmResults.get(0).getTotalC()));

        }
        catch(Exception err)
        {
            Debugger.logD(err.toString());
        }

    }

    private void showEmptyListIndicator1(boolean show)
    {
        emptyIndicator1.setVisibility(show ? View.VISIBLE : View.GONE);
        lvCpd1.setVisibility(show ? View.GONE : View.VISIBLE);
    }


    private void showSearchBar() {
        if (searchBarClicked){
            searchBarClicked = false;
            searchView.setVisibility(View.GONE);
            view_bars.setVisibility(View.VISIBLE);
        }
        else {
            searchBarClicked = true;
            searchView.setVisibility(View.VISIBLE);
            view_bars.setVisibility(View.GONE);

        }

    }

    private void addSeminar()
    {
        if (getActivity() == null) return;
        Intent intent = new Intent(view.getContext(), CpdMonitoringActivity.class);
        startActivity(intent);
    }

    public void syncCpdMonitoring(final Context context)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        try {
            progressDialog.show();
            progressDialog.setMessage("Loading CPD Monitoring");
            if(!Utility.haveNetworkConnection(context)){
                progressDialog.dismiss();
                Toasty.error(context, "Internet connection is required").show();
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/cpdmonitoringapi", entity, true, new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        if (response != null){
                            CpdMonitoring cpdMonitoring1 = new CpdMonitoring();
                            cpdMonitoring1.delete();

                            ArrayList<CpdMonitoring> cpdMonitoringArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<CpdMonitoring>>(){}.getType());
                            for (CpdMonitoring cpdMonitoring : cpdMonitoringArrayList)
                            {
                                cpdMonitoring.save(cpdMonitoring);
                            }
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    readRecords("");
                                }
                            });
//                           ((CpdMonitoringFragment)fragment).readRecords("");
                        }
                    }
                    catch (Exception err)
                    {
                        Debugger.logD(err.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occurred while syncing").show();

                    Debugger.printO("Response String " + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occurred while syncing").show();

                    Debugger.printO("Json Array " + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context, "An error occurred while syncing").show();

                    Debugger.printO("Json Object " + errorResponse);
                }
            });
        }
        catch (Exception err)
        {
            Toast.makeText(context,"Error " + err.toString(),Toast.LENGTH_SHORT).show();
        }
    }

    public void syncCpdMonitoringCompetencies(final Context context)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        try {
            progressDialog.show();
            progressDialog.setMessage("Loading competencies");
            if(!Utility.haveNetworkConnection(context)){
                progressDialog.dismiss();
                Toasty.error(context, "Internet connection is required").show();
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/userscompetency", entity, true, new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        if (response != null){
                            Competencies competencies1 = new Competencies();
                            competencies1.delete();

                            ArrayList<Competencies> competenciesArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<Competencies>>(){}.getType());

                            for (Competencies competencies : competenciesArrayList)
                            {
                                competencies.save(competencies);
                            }

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loadSubTopics();
                                }
                            });
//                            ((CpdMonitoringFragment)fragment).loadSubTopics();
                        }
                    }
                    catch (Exception err)
                    {
                        Debugger.logD(err.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occurred while syncing").show();

                    Debugger.printO("Response String " + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occurred while syncing").show();

                    Debugger.printO("Json Array " + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context, "An error occurred while syncing").show();
                    Debugger.printO("Json Object " + errorResponse);
                }
            });
        }
        catch (Exception err)
        {
            Toasty.warning(context, "An error occurred while syncing").show();
        }
    }

    public void syncCpdMonitorings(final Context context)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        try{
            progressDialog.show();
            progressDialog.setMessage("Loading CPD Monitoring");
            if(!Utility.haveNetworkConnection(context)){
                progressDialog.dismiss();
                Toasty.error(context, "Internet connection is required").show();
            }
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("Token", UserSession.getToken(context));
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "post/userseminarapi", entity, true, new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        if (response.getJSONArray("result") != null){
                            CpdMonitorings cpdMonitorings1 = new CpdMonitorings();
                            cpdMonitorings1.delete();

                            ArrayList<CpdMonitorings> cpdMonitoringArrayList = new Gson().fromJson(response.getJSONArray("result").toString(), new TypeToken<List<CpdMonitorings>>(){}.getType());

                            for (CpdMonitorings cpdMonitorings : cpdMonitoringArrayList)
                            {
                                cpdMonitorings.save(cpdMonitorings);
                            }
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    readRecords1("");
                                }
                            });
//                            ((CpdMonitoringFragment)fragment).readRecords1("");
                        }
                    }
                    catch (Exception err)
                    {
                        Debugger.logD(err.toString());
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occurred while syncing").show();

                    Debugger.printO("Response String " + responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occurred while syncing").show();

                    Debugger.printO("Json Array " + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Toasty.warning(context , "An error occurred while syncing").show();

                    Debugger.printO("Json Object " + errorResponse);
                }
            });
        }
        catch (Exception err)
        {
            Toasty.warning(context , "An error occurred while syncing").show();
        }
    }

}
