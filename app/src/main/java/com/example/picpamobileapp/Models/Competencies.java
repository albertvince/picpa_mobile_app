package com.example.picpamobileapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.picpamobileapp.Utils.Debugger;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class Competencies extends RealmObject implements Parcelable {
    private int totalCPD;
    private int totalA;
    private int totalB;
    private int totalC;

    public static final Creator<Competencies> CREATOR = new Creator<Competencies>() {
        @Override
        public Competencies createFromParcel(Parcel in) {
            return new Competencies(in);
        }

        @Override
        public Competencies[] newArray(int size) {
            return new Competencies[size];
        }
    };

    public int getTotalCPD() {
        return totalCPD;
    }

    public void setTotalCPD(int totalCPD) {
        this.totalCPD = totalCPD;
    }

    public int getTotalA() {
        return totalA;
    }

    public void setTotalA(int totalA) {
        this.totalA = totalA;
    }

    public int getTotalB() {
        return totalB;
    }

    public void setTotalB(int totalB) {
        this.totalB = totalB;
    }

    public int getTotalC() {
        return totalC;
    }

    public void setTotalC(int totalC) {
        this.totalC = totalC;
    }

    public Competencies(){

    }

    protected Competencies(Parcel in) {
        totalCPD = in.readInt();
        totalA = in.readInt();
        totalB = in.readInt();
        totalC = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(totalCPD);
        dest.writeInt(totalA);
        dest.writeInt(totalB);
        dest.writeInt(totalC);
    }

    public void save(final Competencies getCompetencies)
    {
        //Database
        final Realm realm ;
        realm = Realm.getDefaultInstance();

        final Competencies competencies = new Competencies();

        competencies.setTotalCPD(competencies.getTotalCPD());
        competencies.setTotalA(competencies.getTotalA());
        competencies.setTotalB(competencies.getTotalB());
        competencies.setTotalC(competencies.getTotalC());

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(getCompetencies);
            }
        });
    }

    public void delete() {
        //Database
        final Realm realm;
        realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Competencies> realmResults = realm.where(Competencies.class).findAll();
                realmResults.deleteAllFromRealm();
            }
        });
    }
}
