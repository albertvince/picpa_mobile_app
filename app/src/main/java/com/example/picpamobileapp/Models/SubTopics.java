package com.example.picpamobileapp.Models;

import android.app.Activity;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.picpamobileapp.MainActivity;
import com.example.picpamobileapp.Utils.Debugger;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class SubTopics extends RealmObject implements Parcelable {

    @PrimaryKey
    private int seminar_subid;

    private int seminar_id;
    private String seminar_subtopic;
    private String seminar_subdate;
    private int seminar_competency;
    private int seminar_units;

    public SubTopics(int seminar_subid, int seminar_id, String seminar_subtopic, String seminar_subdate, int seminar_competency, int seminar_units){
        this.seminar_subid = seminar_subid;
        this.seminar_id = seminar_id;
        this.seminar_subtopic = seminar_subtopic;
        this.seminar_subdate = seminar_subdate;
        this.seminar_competency = seminar_competency;
        this.seminar_units = seminar_units;
    }

    public SubTopics(){

    }

    protected SubTopics(Parcel in) {
        seminar_subid = in.readInt();
        seminar_id = in.readInt();
        seminar_subtopic = in.readString();
        seminar_subdate = in.readString();
        seminar_competency = in.readInt();
        seminar_units = in.readInt();
    }

    public static final Creator<SubTopics> CREATOR = new Creator<SubTopics>() {
        @Override
        public SubTopics createFromParcel(Parcel in) {
            return new SubTopics(in);
        }

        @Override
        public SubTopics[] newArray(int size) {
            return new SubTopics[size];
        }
    };

    public int getSeminar_id() {
        return seminar_id;
    }

    public void setSeminar_id(int seminar_id) {
        this.seminar_id = seminar_id;
    }

    public String getSeminar_subtopic() {
        return seminar_subtopic;
    }

    public void setSeminar_subtopic(String seminar_subtopic) {
        this.seminar_subtopic = seminar_subtopic;
    }

    public String getSeminar_subdate() {
        return seminar_subdate;
    }

    public void setSeminar_subdate(String seminar_subdate) {
        this.seminar_subdate = seminar_subdate;
    }

    public int getSeminar_competency() {
        return seminar_competency;
    }

    public void setSeminar_competency(int seminar_competency) {
        this.seminar_competency = seminar_competency;
    }

    public int getSeminar_units() {
        return seminar_units;
    }

    public void setSeminar_units(int seminar_units) {
        this.seminar_units = seminar_units;
    }

    public int getSeminar_subid() {
        return seminar_subid;
    }

    public void setSeminar_subid(int seminar_subid) {
        this.seminar_subid = seminar_subid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(seminar_subid);
        dest.writeInt(seminar_id);
        dest.writeString(seminar_subtopic);
        dest.writeString(seminar_subdate);
        dest.writeInt(seminar_competency);
        dest.writeInt(seminar_units);
    }

    public void save(final SubTopics getSubtopics)
    {
        //Database
        final Realm realm ;
        realm = Realm.getDefaultInstance();

        final SubTopics subTopics = new SubTopics();

        subTopics.setSeminar_subid(subTopics.getSeminar_subid());
        subTopics.setSeminar_id(subTopics.getSeminar_id());
        subTopics.setSeminar_subtopic(subTopics.getSeminar_subtopic());
        subTopics.setSeminar_subdate(subTopics.getSeminar_subdate());
        subTopics.setSeminar_competency(subTopics.getSeminar_competency());
        subTopics.setSeminar_units(subTopics.getSeminar_units());

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(getSubtopics);
                Debugger.logD("Sub topics save");
            }
        });
    }

    public void delete() {
        //Database
        final Realm realm;
        realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<SubTopics> realmResults = realm.where(SubTopics.class).findAll();
                realmResults.deleteAllFromRealm();
                Debugger.logD("Successfully deleted.");
            }
        });
    }
}
