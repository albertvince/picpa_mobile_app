package com.example.picpamobileapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.picpamobileapp.Utils.Debugger;
import com.google.gson.annotations.SerializedName;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class Chapter extends RealmObject implements Parcelable {

    @SerializedName("chapter_id")
    private int chapterId;
    @SerializedName("chapter_name")
    private String chapterName;

    @Override
    public String toString() {
        return chapterName;
    }

    public Chapter(){

    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public void save(final Chapter getChapter)
    {

        //Database
        final Realm realm ;
        realm = Realm.getDefaultInstance();

        final Chapter chapter = new Chapter();

        chapter.setChapterId(getChapter.getChapterId());
        chapter.setChapterName(getChapter.getChapterName());

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(chapter);
            }
        });
    }

    public static void delete(){

        //Database
        Realm realm ;
        realm = Realm.getDefaultInstance();

        final RealmResults<Chapter> realmResults = realm.where(Chapter.class).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmResults.deleteAllFromRealm();
            }
        });

    }


    protected Chapter(Parcel in) {
        chapterId = in.readInt();
        chapterName = in.readString();
    }

    public static final Creator<Chapter> CREATOR = new Creator<Chapter>() {
        @Override
        public Chapter createFromParcel(Parcel in) {
            return new Chapter(in);
        }

        @Override
        public Chapter[] newArray(int size) {
            return new Chapter[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(chapterId);
        dest.writeString(chapterName);
    }
}
