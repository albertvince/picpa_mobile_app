package com.example.picpamobileapp.Models;

public class EvaluationQuestion {
    private String questionheader;

    public EvaluationQuestion(String questionheader){
        this.questionheader = questionheader;
    }


    public EvaluationQuestion() {

    }

    public String getQuestionheader() {
        return questionheader;
    }

    public void setQuestionheader(String questionheader) {
        this.questionheader = questionheader;
    }
}
