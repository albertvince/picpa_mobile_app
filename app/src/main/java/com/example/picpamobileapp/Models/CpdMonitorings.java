package com.example.picpamobileapp.Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ScrollView;

import com.example.picpamobileapp.Utils.Debugger;
import com.google.gson.annotations.SerializedName;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class CpdMonitorings extends RealmObject implements Parcelable {
    @PrimaryKey
    @Required
    private String userseminar_id;
    @SerializedName("user_id")
    private String user_id;
    private String seminar_topic;
    private String seminar_speaker;
    private String seminar_date;
    private String seminar_venue;
    private String time_from;
    private String time_to;
    private String certificate;
    private int competency;
    private String seminar_units;

    public CpdMonitorings (String user_id, String seminar_topic, String seminar_date, String seminar_venue,  String time_from, String time_to,
                           String seminar_speaker, String certificate, int competency, String seminar_units){
        this.user_id = user_id;
        this.seminar_topic = seminar_topic;
        this.seminar_date = seminar_date;
        this.seminar_venue = seminar_venue;
        this.time_from = time_from;
        this.time_to = time_to;
        this.seminar_speaker = seminar_speaker;
        this.certificate = certificate;
        this.competency = competency;
        this.seminar_units = seminar_units;
    }
    public CpdMonitorings(){

    }

    protected CpdMonitorings(Parcel in) {
        //setUuid(in.readString());
        userseminar_id = in.readString();
        user_id = in.readString();

        seminar_topic = in.readString();
        seminar_date = in.readString();
        seminar_venue = in.readString();
        time_from = in.readString();
        time_to = in.readString();
        seminar_speaker = in.readString();
        certificate = in.readString();
        competency = in.readInt();
        seminar_units = in.readString();
    }

    public static final Creator<CpdMonitorings> CREATOR = new Creator<CpdMonitorings>() {
        @Override
        public CpdMonitorings createFromParcel(Parcel in) {
            return new CpdMonitorings(in);
        }

        @Override
        public CpdMonitorings[] newArray(int size) {
            return new CpdMonitorings[size];
        }
    };

    public String getUuid() {
        return userseminar_id;
    }

    public void setUuid(String userseminar_id) {
        this.userseminar_id = userseminar_id;
    }

    public String getUser_id(){
        return user_id;
    }

    public void setUser_id(String user_id)
    {
        this.user_id = user_id;
    }

    public String getTopic() {
        return seminar_topic;
    }

    public String getDate() {
        return seminar_date;
    }

    public String getVenue() {
        return seminar_venue;
    }

    public String getTimeFrom() {
        return time_from;
    }

    public String getTimeTo() {
        return time_to;
    }

    public String getSeminarSpeaker() {
        return seminar_speaker;
    }

    public String getCertificate() {
        return certificate;
    }

    public int getCompetency() {
        return competency;
    }

    public String getSeminarUnits() {
        return seminar_units;
    }


    public void setTopic(String seminar_topic) {
        this.seminar_topic = seminar_topic;
    }

    public void setDate(String seminar_date) {
        this.seminar_date = seminar_date;
    }

    public void setVenue(String seminar_venue) {
        this.seminar_venue = seminar_venue;
    }

    public void setTimeFrom(String time_from) {
        this.time_from = time_from;
    }

    public void setTimeto(String time_to) {
        this.time_to = time_to;
    }

    public void setSeminarSpeaker(String seminar_speaker) {
        this.seminar_speaker = seminar_speaker;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public void setCompetency(int competency) {
        this.competency = competency;
    }

    public void setSeminarUnits(String seminar_units) {
        this.seminar_units = seminar_units;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(userseminar_id );
        dest.writeString(user_id);
        dest.writeString(seminar_topic);
        dest.writeString(seminar_date);
        dest.writeString(seminar_venue);
        dest.writeString(time_from);
        dest.writeString(time_to);
        dest.writeString(seminar_speaker);
        dest.writeString(certificate);
        dest.writeInt(competency);
        dest.writeString(seminar_units);
    }
    public void save(final CpdMonitorings getCpd)
    {
        //Database
        final Realm realm ;
        realm = Realm.getDefaultInstance();

        final CpdMonitorings cpdMonitoring = new CpdMonitorings();


        cpdMonitoring.setUuid(getCpd.getUuid());
        cpdMonitoring.setUser_id(getCpd.getUser_id());
        cpdMonitoring.setTopic(getCpd.getTopic());
        cpdMonitoring.setDate(getCpd.getDate());
        cpdMonitoring.setVenue(getCpd.getVenue());
        cpdMonitoring.setTimeFrom(getCpd.getTimeFrom());
        cpdMonitoring.setTimeto(getCpd.getTimeTo());
        cpdMonitoring.setSeminarSpeaker(getCpd.getSeminarSpeaker());
        cpdMonitoring.setCertificate(getCpd.getCertificate());
        cpdMonitoring.setCompetency(getCpd.getCompetency());
        cpdMonitoring.setSeminarUnits(getCpd.getSeminarUnits());

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(getCpd);
            }
        });
    }
    public void delete() {
        //Database
        final Realm realm;
        realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<CpdMonitorings> realmResults = realm.where(CpdMonitorings.class).findAll();
                realmResults.deleteAllFromRealm();
            }
        });
    }

}
