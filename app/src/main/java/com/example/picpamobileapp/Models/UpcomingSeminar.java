package com.example.picpamobileapp.Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;

import com.example.picpamobileapp.Fragments.UpseminarsFragment;
import com.example.picpamobileapp.Utils.Debugger;
import com.google.gson.annotations.SerializedName;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class UpcomingSeminar extends RealmObject implements Parcelable {

    @PrimaryKey
    @SerializedName("seminarID")
    private String seminarID;

    private String newdate;
    private String fromtime;
    private String totime;
    private String seminartopic;
    private String status;
    @SerializedName("is_paid")
    private boolean isPaid;

    public UpcomingSeminar (String seminarID, String newdate, String fromtime, String totime,String seminartopic, String status, boolean isPaid){

        this.seminarID = seminarID;
        this.newdate = newdate;
        this.fromtime = fromtime;
        this.totime = totime;
        this.seminartopic = seminartopic;
        this.status = status;
        this.isPaid = isPaid;

    }

    public UpcomingSeminar() {

    }

    protected UpcomingSeminar(Parcel in) {
        seminarID = in.readString();
        newdate = in.readString();
        fromtime = in.readString();
        totime = in.readString();
        seminartopic = in.readString();
        status = in.readString();
        isPaid = in.readByte() != 0;
    }

    public static final Creator<UpcomingSeminar> CREATOR = new Creator<UpcomingSeminar>() {
        @Override
        public UpcomingSeminar createFromParcel(Parcel in) {
            return new UpcomingSeminar(in);
        }

        @Override
        public UpcomingSeminar[] newArray(int size) {
            return new UpcomingSeminar[size];
        }
    };

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public String getSeminarID() {
        return seminarID;
    }

    public void setSeminarID(String seminarID) {
        this.seminarID = seminarID;
    }

    public String getNewdate() {
        return newdate;
    }

    public String getFromtime() {
        return fromtime;
    }

    public String getTotime() {
        return totime;
    }

    public String getSeminartopic() {
        return seminartopic;
    }

    public String getStatus() {
        return status;
    }

    public void setNewdate(String newdate) {
        this.newdate = newdate;
    }

    public void setFromtime(String fromtime) {
        this.fromtime = fromtime;
    }

    public void setTotime(String totime) {
        this.totime = totime;
    }

    public void setSeminartopic(String seminartopic) {
        this.seminartopic = seminartopic;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(seminarID);
        dest.writeString(newdate);
        dest.writeString(fromtime);
        dest.writeString(totime);
        dest.writeString(seminartopic);
        dest.writeString(status);
        dest.writeByte((byte) (isPaid ? 1 : 0));
    }
    public void save(final UpcomingSeminar getUpSeminar)
    {
        //Database
        final Realm realm ;
        realm = Realm.getDefaultInstance();

        final UpcomingSeminar upcomingSeminar = new UpcomingSeminar();

        upcomingSeminar.setSeminarID(getUpSeminar.getSeminarID());
        upcomingSeminar.setNewdate(getUpSeminar.getNewdate());
        upcomingSeminar.setFromtime(getUpSeminar.getFromtime());
        upcomingSeminar.setTotime(getUpSeminar.getTotime());
        upcomingSeminar.setSeminartopic(getUpSeminar.getSeminartopic());
        upcomingSeminar.setStatus(getUpSeminar.getStatus());
        upcomingSeminar.setPaid(getUpSeminar.isPaid());

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(getUpSeminar);
            }
        });

    }

    public static void delete(){

        //Database
         Realm realm;
        realm = Realm.getDefaultInstance();

        final RealmResults<UpcomingSeminar> realmResults = realm.where(UpcomingSeminar.class).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmResults.deleteAllFromRealm();
                Debugger.logD("delete seminar upcoming");
            }
        });
    }
}
