package com.example.picpamobileapp.Models;

import android.app.Activity;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.picpamobileapp.MainActivity;
import com.example.picpamobileapp.Utils.Debugger;
import com.google.gson.annotations.SerializedName;

import io.realm.Realm;
import io.realm.RealmObject;

public class UserProfile extends RealmObject implements Parcelable {

    private int id;
    private String username;
    private String password;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String suffixes;
    private String contact_no;
    private String facebook_acc;
    private String email_add;
    private String full_address;
    private String register_no;
    private String register_date;
    private String valid_until;
    private String company_name;
    private int sector;
    private String billing_address;
    private String chapter;
    private String user_image;
    @SerializedName("prc_imageid")
    private String prc_image;

    public UserProfile (String username, String password, String first_name,  String middle_name, String last_name, String suffixes, String contact_no,
                        String facebook_acc, String email_add, String full_address, String register_no, String register_date, String valid_until, String company_name,
                        int sector, String billing_address, String user_image, int id, String prc_image){

        this.username = username;
        this.password = password;
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.suffixes = suffixes;
        this.contact_no = contact_no;
        this.facebook_acc = facebook_acc;
        this.email_add = email_add;
        this.full_address = full_address;
        this.register_no = register_no;
        this.register_date = register_date;
        this.valid_until = valid_until;
        this.company_name = company_name;
        this.sector = sector;
        this.billing_address = billing_address;
        this.user_image = user_image;
        this.id = id;
        this.prc_image = prc_image;
    }


    public UserProfile(){

    }
    protected UserProfile(Parcel in) {
        username = in.readString();
        password = in.readString();
        first_name = in.readString();
        middle_name = in.readString();
        last_name = in.readString();
        suffixes = in.readString();
        contact_no = in.readString();
        facebook_acc = in.readString();
        email_add = in.readString();
        full_address = in.readString();
        register_no = in.readString();
        register_date = in.readString();
        valid_until = in.readString();
        company_name = in.readString();
        sector = in.readInt();
        billing_address = in.readString();
        user_image = in.readString();
        id = in.readInt();
        prc_image = in.readString();
    }

    public static final Creator<UserProfile> CREATOR = new Creator<UserProfile>() {
        @Override
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        @Override
        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getSuffixes() {
        return suffixes;
    }

    public void setSuffixes(String suffixes) {
        this.suffixes = suffixes;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getFacebook_acc() {
        return facebook_acc;
    }

    public void setFacebook_acc(String facebook_acc) {
        this.facebook_acc = facebook_acc;
    }

    public String getEmail_add() {
        return email_add;
    }

    public void setEmail_add(String email_add) {
        this.email_add = email_add;
    }

    public String getFull_address() {
        return full_address;
    }

    public void setFull_address(String full_address) {
        this.full_address = full_address;
    }

    public String getRegister_no() {
        return register_no;
    }

    public void setRegister_no(String register_no) {
        this.register_no = register_no;
    }


    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public String getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(String billing_address) {
        this.billing_address = billing_address;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getValid_until() {
        return valid_until;
    }

    public void setValid_until(String valid_until) {
        this.valid_until = valid_until;
    }

    public String getRegister_date() {
        return register_date;
    }

    public void setRegister_date(String register_date) {
        this.register_date = register_date;
    }


    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getPrc_image() {
        return prc_image;
    }

    public void setPrc_image(String prc_image) {
        this.prc_image = prc_image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(password);
        dest.writeString(first_name);
        dest.writeString(middle_name);
        dest.writeString(last_name);
        dest.writeString(suffixes);
        dest.writeString(contact_no);
        dest.writeString(facebook_acc);
        dest.writeString(email_add);
        dest.writeString(full_address);
        dest.writeString(register_no);
        dest.writeString(register_date);
        dest.writeString(valid_until);
        dest.writeString(valid_until);
        dest.writeString(company_name);
        dest.writeInt(sector);
        dest.writeString(billing_address);
        dest.writeString(chapter);
        dest.writeString(user_image);
        dest.writeInt(id);
        dest.writeString(prc_image);
    }

    public void save(final UserProfile getUserprofile, final Activity activity)
    {
        //Database
        final Realm realm ;
        realm = Realm.getDefaultInstance();


        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                UserProfile userProfile = realm.createObject(UserProfile.class);

                userProfile.setId(getUserprofile.getId());
                userProfile.setSector(getUserprofile.getSector());
                userProfile.setChapter(getUserprofile.getChapter());
                userProfile.setSuffixes(getUserprofile.getSuffixes());
                userProfile.setUsername(getUserprofile.getUsername());
                userProfile.setPassword(getUserprofile.getPassword());
                userProfile.setPrc_image(getUserprofile.getPrc_image());
                userProfile.setEmail_add(getUserprofile.getEmail_add());
                userProfile.setLast_name(getUserprofile.getLast_name());
                userProfile.setUser_image(getUserprofile.getUser_image());
                userProfile.setContact_no(getUserprofile.getContact_no());
                userProfile.setFirst_name(getUserprofile.getFirst_name());
                userProfile.setMiddle_name(getUserprofile.getMiddle_name());
                userProfile.setValid_until(getUserprofile.getValid_until());
                userProfile.setRegister_no(getUserprofile.getRegister_no());
                userProfile.setFacebook_acc(getUserprofile.getFacebook_acc());
                userProfile.setFull_address(getUserprofile.getFull_address());
                userProfile.setCompany_name(getUserprofile.getCompany_name());
                userProfile.setRegister_date(getUserprofile.getRegister_date());
                userProfile.setBilling_address(getUserprofile.getBilling_address());

            }

        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                ((MainActivity)(activity)).openHomeFragment();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Debugger.logD("Error UserProfile "+error.getMessage());
            }
        });
    }
}
