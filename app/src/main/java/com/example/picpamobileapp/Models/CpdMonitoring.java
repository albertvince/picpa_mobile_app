package com.example.picpamobileapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.picpamobileapp.Utils.Debugger;
import com.google.gson.annotations.SerializedName;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class CpdMonitoring extends RealmObject implements Parcelable {

    @PrimaryKey
    private int seminar_id;
    private String seminar_topic;
    private String seminar_date;
    private String seminar_venue;
    private String time_from;
    private String time_to;
    private String competencyA;
    private String competencyB;
    private String competencyC;
    private String totalUnits;
    @SerializedName("downloadpdf")
    private String downloadPdf;

    public CpdMonitoring (int seminar_id, String seminar_topic, String seminar_date, String seminar_venue,  String time_from, String time_to, String competencyA,
                          String competencyB, String competencyC, String totalUnits){
        this.seminar_id = seminar_id;
        this.seminar_topic = seminar_topic;
        this.seminar_date = seminar_date;
        this.seminar_venue = seminar_venue;
        this.time_from = time_from;
        this.time_to = time_to;
        this.competencyA = competencyA;
        this.competencyB = competencyB;
        this.competencyC = competencyC;
        this.totalUnits = totalUnits;
    }
    public CpdMonitoring(){

    }

    protected CpdMonitoring(Parcel in) {
        seminar_id= in.readInt();
        seminar_topic = in.readString();
        seminar_date = in.readString();
        seminar_venue = in.readString();
        time_from = in.readString();
        time_to = in.readString();
        competencyA = in.readString();
        competencyB = in.readString();
        competencyC = in.readString();
        totalUnits = in.readString();
        downloadPdf = in.readString();
    }

    public static final Creator<CpdMonitoring> CREATOR = new Creator<CpdMonitoring>() {
        @Override
        public CpdMonitoring createFromParcel(Parcel in) {
            return new CpdMonitoring(in);
        }

        @Override
        public CpdMonitoring[] newArray(int size) {
            return new CpdMonitoring[size];
        }
    };

    public String getDownloadPdf() {
        return downloadPdf;
    }

    public void setDownloadPdf(String downloadPdf) {
        this.downloadPdf = downloadPdf;
    }

    public int getId() {
        return seminar_id;
    }

    public void setId(int seminar_id) {
        this.seminar_id = seminar_id;
    }

    public String getTopic() {
        return seminar_topic;
    }

    public String getDate() {
        return seminar_date;
    }

    public String getVenue() {
        return seminar_venue;
    }

    public String getTimeFrom() {
        return time_from;
    }

    public String getTimeTo() {
        return time_to;
    }

    public void setTopic(String seminar_topic) {
        this.seminar_topic = seminar_topic;
    }

    public void setDate(String seminar_date) {
        this.seminar_date = seminar_date;
    }

    public void setVenue(String seminar_venue) {
        this.seminar_venue = seminar_venue;
    }

    public void setTimeFrom(String time_from) {
        this.time_from = time_from;
    }

    public void setTimeto(String time_to) {
        this.time_to = time_to;
    }


    public String getCompetencyA() {
        return competencyA;
    }

    public void setCompetencyA(String competencyA) {
        this.competencyA = competencyA;
    }

    public String getCompetencyB() {
        return competencyB;
    }

    public void setCompetencyB(String competencyB) {
        this.competencyB = competencyB;
    }

    public String getCompetencyC() {
        return competencyC;
    }

    public void setCompetencyC(String competencyC) {
        this.competencyC = competencyC;
    }

    public String getTotalUnits() {
        return totalUnits;
    }

    public void setTotalUnits(String totalUnits) {
        this.totalUnits = totalUnits;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(seminar_id);
        dest.writeString(seminar_topic);
        dest.writeString(seminar_date);
        dest.writeString(seminar_venue);
        dest.writeString(time_from);
        dest.writeString(time_to);
        dest.writeString(competencyA);
        dest.writeString(competencyB);
        dest.writeString(competencyC);
        dest.writeString(totalUnits);
        dest.writeString(downloadPdf);
    }
    public void save(final CpdMonitoring getCpd)
    {
        //Database
        final Realm realm ;
        realm = Realm.getDefaultInstance();

        final CpdMonitoring cpdMonitoring = new CpdMonitoring();

        cpdMonitoring.setId(getCpd.getId());
        cpdMonitoring.setDate(getCpd.getDate());
        cpdMonitoring.setVenue(getCpd.getVenue());
        cpdMonitoring.setTopic(getCpd.getTopic());
        cpdMonitoring.setTimeto(getCpd.getTimeTo());
        cpdMonitoring.setTimeFrom(getCpd.getTimeFrom());
        cpdMonitoring.setTotalUnits(getCpd.getTotalUnits());
        cpdMonitoring.setCompetencyA(getCpd.getCompetencyA());
        cpdMonitoring.setCompetencyB(getCpd.getCompetencyB());
        cpdMonitoring.setCompetencyC(getCpd.getCompetencyC());
        cpdMonitoring.setDownloadPdf(getCpd.getDownloadPdf());

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(getCpd);
            }
        });

    }

    public void delete(){
        //Database
        final Realm realm;
        realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<CpdMonitoring> realmResults = realm.where(CpdMonitoring.class).findAll();
                realmResults.deleteAllFromRealm();
            }
        });
    }




}
