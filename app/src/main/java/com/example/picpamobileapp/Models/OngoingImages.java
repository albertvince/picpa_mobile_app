package com.example.picpamobileapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.picpamobileapp.Utils.Debugger;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class OngoingImages extends RealmObject implements Parcelable {
    private String image_name;

    public OngoingImages (String image_name){
        this.image_name = image_name;

    }

    public OngoingImages(){

    }
    protected OngoingImages(Parcel in) {
        image_name = in.readString();
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public static final Creator<OngoingImages> CREATOR = new Creator<OngoingImages>() {
        @Override
        public OngoingImages createFromParcel(Parcel in) {
            return new OngoingImages(in);
        }

        @Override
        public OngoingImages[] newArray(int size) {
            return new OngoingImages[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image_name);
    }

    public void save(final OngoingImages getOnImages) {
        //Database
        final Realm realm;
        realm = Realm.getDefaultInstance();

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                OngoingImages ongoingImages = realm.createObject(OngoingImages.class);
                ongoingImages.setImage_name(getOnImages.getImage_name());
            }

        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Debugger.logD("Ongoing Images save");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Debugger.logD("Error Upcoming Images "+error.getMessage());
            }
        });
    }
    public void delete(){
        //Database
        final Realm realm;
        realm = Realm.getDefaultInstance();

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<OngoingImages> realmResults = realm.where(OngoingImages.class).findAll();
                realmResults.deleteAllFromRealm();

//                UpcomingImages upcomingImages = realm.deleteAll(UpcomingImages);
//                upcomingImages.setImage_name(delUpImages.getImage_name());
            }

        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Debugger.logD("Ongoing Images deleted");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Debugger.logD("Error Upcoming Images "+error.getMessage());
            }
        });
    }

}
