package com.example.picpamobileapp.Models;

import android.os.Parcel;
import android.os.Parcelable;


import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class Speakers extends RealmObject implements Parcelable {
    @PrimaryKey
    private int speakerID;
    private int seminarID;
    private String seminarspeaker;
    private String seminartopic;


    public Speakers (int speakerID, int seminarID, String seminarspeaker, String seminartopic){
        this.speakerID = speakerID;
        this.seminarID = seminarID;
        this.seminarspeaker = seminarspeaker;
        this.seminartopic = seminartopic;

    }

    public Speakers() {

    }


    protected Speakers(Parcel in) {
        seminarID = in.readInt();
        speakerID = in.readInt();
        seminarspeaker = in.readString();
        seminartopic = in.readString();
    }


    public int getSeminarID() {
        return seminarID;
    }

    public void setSeminarID(int seminarID) {
        this.seminarID = seminarID;
    }

    public int getSpeakerID() {
        return speakerID;
    }

    public void setSpeakerID(int speakerID) {
        this.speakerID = speakerID;
    }

    public String getSeminarspeaker() {
        return seminarspeaker;
    }

    public void setSeminarspeaker(String seminarspeaker) {
        this.seminarspeaker = seminarspeaker;
    }

    public String getSeminartopic() {
        return seminartopic;
    }

    public void setSeminartopic(String seminartopic) {
        this.seminartopic = seminartopic;
    }


    public static final Creator<Speakers> CREATOR = new Creator<Speakers>() {
        @Override
        public Speakers createFromParcel(Parcel in) {
            return new Speakers(in);
        }

        @Override
        public Speakers[] newArray(int size) {
            return new Speakers[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(seminarID);
        dest.writeInt(speakerID);
        dest.writeString(seminarspeaker);
        dest.writeString(seminartopic);

    }
    public void save(final Speakers getSpeakers)
    {
        //Database
        final Realm realm ;
        realm = Realm.getDefaultInstance();

        final Speakers speakers = new Speakers();

        speakers.setSeminarID(getSpeakers.getSeminarID());
        speakers.setSeminarspeaker(getSpeakers.getSeminarspeaker());
        speakers.setSpeakerID(getSpeakers.getSpeakerID());
        speakers.setSeminartopic(getSpeakers.getSeminartopic());

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(getSpeakers);
            }
        });
    }

    public void delete() {
        //Database
        final Realm realm;
        realm = Realm.getDefaultInstance();

        final RealmResults<Speakers> realmResults = realm.where(Speakers.class).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmResults.deleteAllFromRealm();
            }
        });
    }

}