package com.example.picpamobileapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.picpamobileapp.Utils.Debugger;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class UpcomingImages extends RealmObject implements Parcelable {

    private String image_name;

    public UpcomingImages (String image_name, int upimage_id){
        this.image_name = image_name;

    }

    public UpcomingImages() {

    }

    protected UpcomingImages(Parcel in) {
        image_name = in.readString();
    }


    public void setUpimage_id(String upimage_id) {
        this.image_name = upimage_id;
    }
    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }


    public static final Creator<UpcomingImages> CREATOR = new Creator<UpcomingImages>() {
        @Override
        public UpcomingImages createFromParcel(Parcel in) {
            return new UpcomingImages(in);
        }

        @Override
        public UpcomingImages[] newArray(int size) {
            return new UpcomingImages[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image_name);
    }

    public void save(final UpcomingImages getUpImages) {
        //Database
        final Realm realm;
        realm = Realm.getDefaultInstance();

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                UpcomingImages upcomingImages = realm.createObject(UpcomingImages.class);
                upcomingImages.setImage_name(getUpImages.getImage_name());
            }

        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Debugger.logD("Upcoming Images save");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Debugger.logD("Error Upcoming Images "+error.getMessage());
            }
        });
    }
    public void delete(){
        //Database
        final Realm realm;
        realm = Realm.getDefaultInstance();

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<UpcomingImages> realmResults = realm.where(UpcomingImages.class).findAll();
                realmResults.deleteAllFromRealm();

//                UpcomingImages upcomingImages = realm.deleteAll(UpcomingImages);
//                upcomingImages.setImage_name(delUpImages.getImage_name());
            }

        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Debugger.logD("Upcoming Images deleted");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Debugger.logD("Error Upcoming Images "+error.getMessage());
            }
        });
    }

}
