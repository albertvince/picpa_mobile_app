package com.example.picpamobileapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.picpamobileapp.Utils.Debugger;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class Evaluation extends RealmObject implements Parcelable {
    @PrimaryKey
    private int seminar_id;

    private String seminar_topic;
    private String seminar_date;
    private String seminar_venue;
    private String time_from;
    private String time_to;

    public Evaluation (int seminar_id, String seminar_topic, String seminar_date, String seminar_venue,  String time_from, String time_to){
        this.seminar_id = seminar_id;
        this.seminar_topic = seminar_topic;
        this.seminar_date = seminar_date;
        this.seminar_venue = seminar_venue;
        this.time_from = time_from;
        this.time_to = time_to;

    }

    public Evaluation() {

    }


    protected Evaluation(Parcel in) {
        seminar_id = in.readInt();
        seminar_topic = in.readString();
        seminar_date = in.readString();
        seminar_venue = in.readString();
        time_from = in.readString();
        time_to = in.readString();
    }

    public static final Creator<Evaluation> CREATOR = new Creator<Evaluation>() {
        @Override
        public Evaluation createFromParcel(Parcel in) {
            return new Evaluation(in);
        }

        @Override
        public Evaluation[] newArray(int size) {
            return new Evaluation[size];
        }
    };

    public int getId() {
        return seminar_id;
    }

    public void setId(int seminar_id) {
        this.seminar_id = seminar_id;
    }

    public String getTopic() {
        return seminar_topic;
    }

    public String getDate() {
        return seminar_date;
    }

    public String getVenue() {
        return seminar_venue;
    }

    public String getTimeFrom() {
        return time_from;
    }

    public String getTimeTo() {
        return time_to;
    }

    public void setTopic(String seminar_topic) {
        this.seminar_topic = seminar_topic;
    }

    public void setDate(String seminar_date) {
        this.seminar_date = seminar_date;
    }

    public void setVenue(String seminar_venue) {
        this.seminar_venue = seminar_venue;
    }

    public void setTimeFrom(String time_from) {
        this.time_from = time_from;
    }

    public void setTimeto(String time_to) {
        this.time_to = time_to;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(seminar_id);
        dest.writeString(seminar_topic);
        dest.writeString(seminar_date);
        dest.writeString(seminar_venue);
        dest.writeString(time_from);
        dest.writeString(time_to);
    }
    public void save(final Evaluation getEvaluation)
    {
//        deleteSpeakers();

        //Database
        final Realm realm ;
        realm = Realm.getDefaultInstance();

        final Evaluation evaluation = new Evaluation();

        evaluation.setId(getEvaluation.getId());
        evaluation.setTopic(getEvaluation.getTopic());
        evaluation.setDate(getEvaluation.getDate());
        evaluation.setVenue(getEvaluation.getVenue());
        evaluation.setTimeFrom(getEvaluation.getTimeFrom());
        evaluation.setTimeto(getEvaluation.getTimeTo());

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(getEvaluation);
            }
        });

    }

    private void deleteSpeakers(){
        final Realm realm ;
        realm = Realm.getDefaultInstance();

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<Speakers> speakersRealmResults = realm.where(Speakers.class).findAll();
                    speakersRealmResults.deleteAllFromRealm();
                }
            });
        } finally {
            realm.close();
        }
    }

    public void delete() {
        //Database
        final Realm realm;
        realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Evaluation> realmResults = realm.where(Evaluation.class).findAll();
                realmResults.deleteAllFromRealm();
            }
        });
    }

}
