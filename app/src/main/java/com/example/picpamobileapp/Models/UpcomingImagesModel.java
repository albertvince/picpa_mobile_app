package com.example.picpamobileapp.Models;

import com.google.gson.annotations.SerializedName;

public class UpcomingImagesModel {

    @SerializedName("image_name")
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
