package com.example.picpamobileapp.Interfaces;

import android.view.View;

public interface RecyclerViewClick {
    public void onItemClick(View v, int position);
}
