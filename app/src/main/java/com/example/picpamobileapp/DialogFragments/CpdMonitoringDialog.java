package com.example.picpamobileapp.DialogFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.picpamobileapp.Models.CpdMonitoring;
import com.example.picpamobileapp.R;
import com.example.picpamobileapp.Utils.Debugger;
import com.example.picpamobileapp.Utils.HttpProvider;
import com.example.picpamobileapp.Utils.Utility;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class CpdMonitoringDialog extends DialogFragment {


    //Context
    private Context context;
    private View view;

    //Widgets
    private Button btnDownload;
    private TextView tvSeminarTitle, tvDateTime, tvCompetencyA, tvCompetencyB, tvCompetencyC;

    //Variable
    private String fileName = "certificate.pdf";

    //Data
    private CpdMonitoring cpdMonitoring;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.custom_cpdmonitoring_dialog, container, false);
        context = view.getContext();

        if (getArguments() != null)
        {
            cpdMonitoring = getArguments().getParcelable("MONITORING_OBJECT");
            initializeUI();
        }

        return view;
    }

    private void initializeUI(){

        tvDateTime = view.findViewById(R.id.tv_date_time);
        tvCompetencyA = view.findViewById(R.id.tv_competency_a);
        tvCompetencyB = view.findViewById(R.id.tv_competency_b);
        tvCompetencyC = view.findViewById(R.id.tv_competency_c);
        tvSeminarTitle = view.findViewById(R.id.tv_seminar_title);
        btnDownload = view.findViewById(R.id.btn_download_certificate);


        tvSeminarTitle.setText(cpdMonitoring.getTopic());
        tvCompetencyA.setText(cpdMonitoring.getCompetencyA());
        tvCompetencyB.setText(cpdMonitoring.getCompetencyB());
        tvCompetencyC.setText(cpdMonitoring.getCompetencyC());
        tvDateTime.setText(cpdMonitoring.getDate() + "\n"+ cpdMonitoring.getTimeFrom() +" - "+cpdMonitoring.getTimeTo());

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utility.haveNetworkConnection(context, getFragmentManager()))
                    new DownloadCertificate(context, cpdMonitoring.getDownloadPdf()).execute();
            }
        });

    }


    private class DownloadCertificate  extends AsyncTask<String, Integer, String> {

        private Context context;
        private ProgressDialog progressDialog;
        private String success = "", errorMessage = "", urlToDownload;

        public DownloadCertificate(Context context, String urlToDownload) {
            this.context = context;
            this.urlToDownload = urlToDownload;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Downloading...");
            progressDialog.show();

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                HttpProvider.getFile(context, "https://registration.picpacdomisor.org/user/backup?"+urlToDownload , null, new FileAsyncHttpResponseHandler(context) {

                    @Override
                    public void onStart() {
                        super.onStart();
                        errorMessage = "";
                    }

                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                        errorMessage = context.getResources().getString(R.string.server_error);
                    }

                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, File file) {

                        dismiss();
                        if(moveCacheFile(context, file, fileName))
                        {
                            viewFile(fileName);
                        }
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        if(errorMessage.equals("")){
                            success += "";
                        }else {
                            success += "FAILED";
                        }
                    }
                });

            } catch (Exception err) {
                errorMessage = "doInBackground Exception " + err;
                Debugger.logD(errorMessage);
                return success += "FAILED";
            }

            return success;

        }

        @Override
        protected void onPostExecute(String success) {

            if (success.isEmpty()) {
                Toasty.success(context, "Successfully download").show();
            } else {
                Toasty.warning(context, "Failed to download!").show();
            }

            progressDialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values)
            ;
        }
    }

    //We can't read the file in cache because don't have the extension(.pdf or .xlsx)
    //To have an extension(.pdf or .xlsx) we need to move it to other file
    //When moving the file we put the extension(.pdf or .xlsx) so we can read/write the file
    public static boolean moveCacheFile(Context context, File cacheFile, String internalStorageName) {

        boolean isMove = false;
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;

        try {
            inputStream = new FileInputStream(cacheFile.getAbsoluteFile());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
            byte[] buffer = new byte[1024];
            int read = -1;

            while ((read = inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, read);
            }

            byteArrayOutputStream.close();
            inputStream.close();

            outputStream = context.openFileOutput(internalStorageName, Context.MODE_PRIVATE);
            byteArrayOutputStream.writeTo(outputStream);

            outputStream.close();

            //delete cache files after download
            cacheFile.delete();

            isMove = true;
        } catch (Exception e) {
            Toasty.warning(context, e.toString(), Toast.LENGTH_SHORT).show();
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
            }
            try {
                if (outputStream != null) outputStream.close();
            } catch (IOException e) {
            }
        }

        return isMove;
    }

    //View the file
    private void viewFile(String fileName){

        try{

            File myFiles = new File(context.getFilesDir() + "/"+fileName);
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            Uri uri = FileProvider.getUriForFile(context, "com.yahshua.picpamobileapp", myFiles);
            intent.setDataAndType(uri, getMimeType(myFiles.getPath()));
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(intent);

        }catch (Exception err){
            Toasty.warning(context, "Please install excel/pdf reader", Toast.LENGTH_LONG).show();
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        super.onResume();
    }

    public int getTheme() {
        return R.style.full_screen_dialog;
    }
}
